﻿using System;
using System.IO;

namespace GrapeSeedInvoice.Helper
{
    public class FileHelper
    {
        public static bool IsExisted(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                return false;

            var f = new FileInfo(filePath);
            return f.Exists;
        }

        public static bool IsOpened(string filePath)
        {
            try
            {
                Stream s = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None);

                s.Close();

                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
