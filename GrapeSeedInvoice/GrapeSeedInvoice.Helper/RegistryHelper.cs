﻿using Microsoft.Win32;
using Newtonsoft.Json;

namespace GrapeSeedInvoice.Helper
{
    public class RegistryHelper
    {
        public static void SetValue(string key, string value)
        {
            var registry = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\GrapeseedInvoices");

            //storing the values  
            if (registry != null)
            {
                registry.SetValue(key, value);
                registry.Close();
            }
        }

        public static string GetString(string key, string defaultValue = "")
        {
            var registry = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\GrapeseedInvoices");
            var setting = registry?.GetValue(key);
            if (string.IsNullOrWhiteSpace(setting?.ToString()))
            {
                return defaultValue;
            }

            return setting?.ToString();
        }

        public static int GetInt(string key, int defaultValue = 0)
        {
            var str = GetString(key);
            if (!string.IsNullOrWhiteSpace(str))
                return defaultValue;

            int.TryParse(str, out var output);
            return output;
        }

        public static bool GetBool(string key, bool status = false)
        {
            bool.TryParse(GetString(key, status.ToString()), out bool result);
            return result;
        }

        public static T GetObject<T>(string key, T defaultObject = default(T))
        {
            var json = GetString(key);
            if (string.IsNullOrWhiteSpace(json))
                return defaultObject;

            return JsonConvert.DeserializeObject<T>(json);
        }  
    }
}
