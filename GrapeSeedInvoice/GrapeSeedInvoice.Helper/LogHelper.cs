﻿using System;
using System.IO;

namespace GrapeSeedInvoice.Helper
{
    public class LogHelper
    {
        public static void LogMessage(string msg)
        {
            var sFilePath = GetLogFile(DateTime.Now);

            var sw = System.IO.File.AppendText(sFilePath);
            try
            {
                var logLine = $"{System.DateTime.Now:G}: {msg}";
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }

        public static string GetLogFolderPath()
        {
            var path = Environment.CurrentDirectory + "\\Logs";
            var dir = new DirectoryInfo(path);
            if(!dir.Exists)
                dir.Create();

            return path;
        }

        public static string GetLogFile(DateTime date)
        {
            var friendlyName = System.AppDomain.CurrentDomain.FriendlyName;
            friendlyName = friendlyName.Substring(0, friendlyName.LastIndexOf(".", StringComparison.Ordinal));

            var folderLog = GetLogFolderPath();
            
            var sFilePath = $"{folderLog}\\Log_{date.Year}_{date.Month}_{date.Day}_" + friendlyName + ".txt";
            return sFilePath;
        }
    }
}
