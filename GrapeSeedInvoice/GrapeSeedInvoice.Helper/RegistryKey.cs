﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Helper
{
    public class RegistryKey
    {
        public const string IgnoreAccountManagers = "IGNORE_ACCOUNT_MANAGERS";

        public const string InvoiceTemplate = "INVOICE_TEMPLATE";
        public const string LicenseFile = "LICENSE_FILE";
        public const string OutputInvoiceFolder = "OUTPUT_FOLDER";
        public const string UsdExchangeRate = "USD_EXCHANGE";
        public const string UseTestApiServer = "USE_TEST_API_SERVER";
        public const string RememberMe = "REMEMBER_ME";
        public const string UserName = "USER_NAME";
        public const string Password = "PASSWORD";
        public const string ZipFolder = "ZIP_FOLDER";

        public const string GrapeSeedPrice = "GrapeSeed_Price";
        public const string LittleSeedPrice = "LittleSeed_Price";
        public const string ExcludeMaterialProducts = "ExcludeMaterialProducts";
        
        public const string InvoiceTemplateColumnRowSetting = "InvoiceTemplateColumnRowSetting";
        public const string VietnamLicenseColumnRowSetting = "VietnamLicenseColumnRowSetting";
    }
}
