﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Helper
{
    public class DefaultConst
    {
        public const float GrapeSeedPrice = 212;

        public const float LittleSeedPrice = 330;

        public const string IgnoreAccountMangers = "zGSVN Admin";
    }
}
