﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Helper
{
    public class InvoiceDefaultSetting
    {
        public const string InvoiceDateColumn = "B";
        public const int InvoiceDateRow = 6;

        public const string SchoolColumn = "B";
        public const int SchoolRow = 7;

        public const string AddressColumn = "B";
        public const int AddressRow = 8;
    }
}
