﻿using System.Linq;

namespace GrapeSeedInvoice.Helper
{
    public static class StringExtensions
    {
        public static bool IsNegativeNumber(this string text)
        {
            if (int.TryParse(text, out int number))
            {
                return number < 0;
            }

            return false;
        }

        public static string RemoveVietnameseSign(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.ToLower();

            var strs = text.Trim()
                                          .Split(' ')
                                          .Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim());

            text = string.Join(" ", strs);

            var arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "đ",
                "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
                "í","ì","ỉ","ĩ","ị",
                "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
                "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
                "ý","ỳ","ỷ","ỹ","ỵ",};

            var arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
                "d",
                "e","e","e","e","e","e","e","e","e","e","e",
                "i","i","i","i","i",
                "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
                "u","u","u","u","u","u","u","u","u","u","u",
                "y","y","y","y","y",};

            for (var i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]).Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }

            return text.Trim();
        }
    }
}
