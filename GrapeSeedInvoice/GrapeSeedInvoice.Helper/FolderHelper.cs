﻿using System;
using System.IO;
using System.Linq;

namespace GrapeSeedInvoice.Helper
{
    public class FolderHelper
    {
        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        /// <summary>
        /// Create a subfolder in current program folder
        /// Tạo 1 thư mục con trong thư mục chứa app
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public static string CreateCurrentSubfolder(string folderName)
        {
            var path = Environment.CurrentDirectory + $"\\{folderName}";
            var dir = new DirectoryInfo(path);
            if (!dir.Exists)
                dir.Create();

            return path;
        }

        public static bool CreateFolder(string folderPath, bool deleteAndCreateAgainIfExisted = true)
        {
            var dir = new DirectoryInfo(folderPath);
            if (!dir.Exists)
            {
                dir.Create();
                return true;
            }

            if (dir.Exists && deleteAndCreateAgainIfExisted)
            {
                dir.Delete(true);
                dir.Create();
            }

            return true;
        }

        public static bool IsExisted(string folderPath)
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return false;

            var dir = new DirectoryInfo(folderPath);
            return dir.Exists;
        }
    }
}
