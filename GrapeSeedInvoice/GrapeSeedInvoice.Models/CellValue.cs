﻿namespace GrapeSeedInvoice.Models
{
    public class CellValue
    {
        public CellValue()
        {
            
        }

        public CellValue(int line, string column, string value)
        {
            this.Line = line;
            this.Column = column;
            this.Value = value;
        }

        /// <summary>
        /// B, C, D...
        /// </summary>
        public string Column { get; set; }

        public int Line { get; set; }

        public string Value { get; set; }

        public string Formula { get; set; }

        public string NumberFormat { get; set; }
    }
}
