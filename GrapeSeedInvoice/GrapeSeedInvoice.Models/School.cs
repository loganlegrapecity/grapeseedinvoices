﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Models
{
    public class School
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public InvoiceData InvoiceData { get; set; }
    }
}
