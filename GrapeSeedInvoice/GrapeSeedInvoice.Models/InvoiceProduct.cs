﻿namespace GrapeSeedInvoice.Models
{
    public class InvoiceProduct
    {
        public InvoiceProduct()
        {
            
        }

        public InvoiceProduct(string productName, int quantity, float price)
        {
            this.ProductName = productName;
            this.Quantity = quantity;
            this.Price = price;
        }

        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public float Price { get; set; }

        public float TotalPrice { get; set; }

        public float Adjustment { get; set; }
    }
}
