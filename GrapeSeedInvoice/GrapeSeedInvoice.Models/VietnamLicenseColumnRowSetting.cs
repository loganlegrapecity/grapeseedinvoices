﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Models
{
    public class VietnamLicenseColumnRowSetting
    {
        public string SchoolColumn { get; set; } = "B";

        public string CampusColumn { get; set; } = "C";

        public string ClassColumn { get; set; } = "D";

        public int StartRowIndex { get; set; } = 5;

        public string CurrentUnitColumn { get; set; } = "E";

        public string TotalBilledColumn { get; set; } = "J";

        public string LittleSeedColumn { get; set; } = "K";

        public string TotalAdjustment { get; set; } = "I";

        public int SheetIndex { get; set; } = 2;
    }
}
