﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrapeSeedInvoice.Models
{
    public class InvoiceData
    {
        public string InvoiceDate { get; set; }

        public string SchoolName { get; set; }

        public string Address { get; set; }

        public List<InvoiceProduct> Products { get; set; }

        public float UsdExchangeRate { get; set; }
    }
}
