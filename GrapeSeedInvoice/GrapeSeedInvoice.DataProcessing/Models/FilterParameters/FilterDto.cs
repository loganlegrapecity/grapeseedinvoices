﻿namespace GrapeSeedInvoice.DataProcessing.Models.FilterParameters
{
    public class FilterDto
    {
        public bool Disabled { get; set; }
    }
}