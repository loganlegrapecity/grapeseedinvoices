﻿using System;

namespace GrapeSeedInvoice.DataProcessing.Models.FilterParameters
{
    public class CampusRequestDto
    {

        public CampusRequestDto(Guid schoolId)
        {
            this.Include = "schoolId,campusId,isPrimary,name,campusNumber,addressLine1,addressLine2";
            this.Filter = new FilterDto()
            {
                Disabled = false
            };
            this.SchoolId = schoolId;
        }
        
        //"schoolId,campusId,isPrimary,name,campusNumber,phone,fax,state,city,postal
        //Code,addressLine1,addressLine2,disabled,campusNumber"
        public string Include { get; set; }
        
        public FilterDto Filter { get; set; }

        public Guid? SchoolId { get; set; }
        
        // public Guid[] CampusIds { get; set; }
    }
}