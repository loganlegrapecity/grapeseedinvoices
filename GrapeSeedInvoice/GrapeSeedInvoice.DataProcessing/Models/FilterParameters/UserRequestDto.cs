﻿using System.Collections.Generic;

namespace GrapeSeedInvoice.DataProcessing.Models.FilterParameters
{
    public class UserRequestDto
    {
        public UserRequestDto()
        {
            this.Filter = new FilterDto();
            Roles = new List<string>()
            {
                // "SchoolAdmin", 
                // "CampusAdmin", 
                // "Teacher", 
                // "AccountManager",
                // "TrainingManager",
                // "SystemAdmin",
                // "GlobalHead", 
                // "Trainer",
                // "TrainingAdmin", 
                // "RegionAdmin",
                // "Teacher"
                "AccountManager",
                "SchoolAdmin",
                "CampusAdmin",
                "Teacher",
                "Coach",
            };
        }

        public List<string> Roles { get; set; }

        public List<string> UserIds { get; set; }

        public string Include { get; set; } = "userid, name, email, phone, roles";

        public FilterDto Filter { get; set; }
    }
}