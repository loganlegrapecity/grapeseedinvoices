﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class SchoolListDto
    {
        public List<SchoolDto> Schools { get; set; }
    }

    public class SchoolDto
    {
        public string id { get; set; }

        public string name { get; set; }

        public string gsNumber { get; set; }

        public string trainerName { get; set; }

        public string campusName { get; set; }

        [JsonIgnore]
        public string SchoolAddress { get; set; }

        [JsonIgnore]
        public List<string> AccountManagerIds { get; set; }
    }
}