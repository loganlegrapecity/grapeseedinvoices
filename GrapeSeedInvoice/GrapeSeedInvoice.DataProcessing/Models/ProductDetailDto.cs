﻿using System.Collections.Generic;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class ProductDetailDto
    {
        public string id { get; set; }
        public string name { get; set; }
        public int? startUnit { get; set; }
        public int? endUnit { get; set; }
        public List<UnitPrice> unitPrices { get; set; }
       // public string globalProduct { get; set; }
        public bool isTextbookSubscription { get; set; }
        public bool isDigitalSubscription { get; set; }
        public bool isPurchase { get; set; }
        public bool disabled { get; set; }
        public bool isPackageSubProduct { get; set; }
        public List<SubProduct> subProducts { get; set; }
        public string regionId { get; set; }
    }
    public class UnitPrice
    {
        public string id { get; set; }
        public double price { get; set; }
        public int? startUnit { get; set; }
        public int? endUnit { get; set; }
    }

    public class SubProduct
    {
        public int startUnit { get; set; }
        public int endUnit { get; set; }
        public bool disabled { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    } 
}
