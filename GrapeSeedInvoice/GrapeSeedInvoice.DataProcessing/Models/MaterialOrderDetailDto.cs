﻿using System;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class MaterialOrderDetailDto
    {
        public DateTime? ShippedDate { get; set; }
    }
}
