﻿using System;
using System.Collections.Generic;

namespace GrapeSeedInvoice.DataProcessing.Models
{
   public class MaterialProductDto
    {
        public DateTime submissionDate { get; set; }

        public string orderId { get; set; }

        public string schoolId { get; set; }

        public string schoolName { get; set; }

        public string regionId { get; set; }

        public string regionName { get; set; }

        public string campusId { get; set; }

        public string campusName { get; set; }

        public int status { get; set; }

        public int materialRequestId { get; set; }

        public List<SchoolClassMaterialOrderProduct> schoolClassMaterialOrderProduct { get; set; }

        public string id { get; set; }
    }

   public class MaterialOrderProduct
   {
       public string productId { get; set; }
       
       public string productName { get; set; }
       
       public int? productUnit { get; set; }

       public int quantity { get; set; }

       public int productType { get; set; }
   }

   public class SchoolClassMaterialOrderProduct
   {
       public string schoolClassId { get; set; }
       public string schoolClassName { get; set; }
       public List<MaterialOrderProduct> materialOrderProduct { get; set; }
   }
}
