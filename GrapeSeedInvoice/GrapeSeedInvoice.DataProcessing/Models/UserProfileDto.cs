﻿using System.Collections.Generic;
using System.IO.Pipes;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class UserProfileDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool? Disabled { get; set; }

        public List<string> Roles { get; set; }
        
        public List<string> RegionIds { get; set; }
    }
}