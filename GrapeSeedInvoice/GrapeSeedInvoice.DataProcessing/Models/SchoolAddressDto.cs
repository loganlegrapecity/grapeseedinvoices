﻿namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class SchoolAddressDto
    {
        public string id { get; set; }

        public string name { get; set; }

        public string postalCode { get; set; }

        public string state { get; set; }

        public string city { get; set; }

        public string address1 { get; set; }

        public string address2 { get; set; }

        public string phone { get; set; }

        public string fax { get; set; }

        public bool isPrimary { get; set; }

        public bool disabled { get; set; }

        public string gsNumber { get; set; }

        public bool annualPrepComplete { get; set; }
    }
}
