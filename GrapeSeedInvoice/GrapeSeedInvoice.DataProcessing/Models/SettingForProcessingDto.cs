﻿using System;
using System.Collections.Generic;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class SettingForProcessingDto
    {
        public float UsdExchangeRate { get; set; }

        public bool IsTestEnv { get; set; }

        public string LicenseExcelFile { get; set; }

        public List<SchoolDto> SpecificSchools { get; set; }

        public DateTime StartShippedDate { get; set; }

        public DateTime EndShippedDate { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}