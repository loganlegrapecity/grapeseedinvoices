﻿namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class VietnamRegionLicenseReport
    {
        public string SchoolName { get; set; }

        public string CampusName { get; set; }

        public string ClassName { get; set; }
        
        public string UnitNumber { get; set; }

        public string LittleSEEDLicense { get; set; }

        public string TotalBilled { get; set; }

        public string TotalAdjustment { get; set; }
    }
}
