﻿using System.Collections.Generic;

namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class AccountDto
    {
        public string UserId { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public List<string> Roles { get; set; }
    }
}
