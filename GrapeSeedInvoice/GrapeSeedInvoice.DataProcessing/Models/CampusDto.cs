﻿namespace GrapeSeedInvoice.DataProcessing.Models
{
    public class CampusDto
    {
        public string SchoolId { get; set; }

        public string CampusId { get; set; }

        public bool IsPrimary { get; set; }
        public string Name { get; set; }
        
        /// <summary>
        /// It's stored as number(month) in database (only for VN region)
        /// </summary>
        public string CampusNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }
    }
}
