﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GrapeSeedInvoice.DataProcessing.Models;
using GrapeSeedInvoice.Helper;
using GrapeSeedInvoice.Models;
using Excel = Microsoft.Office.Interop.Excel;

namespace GrapeSeedInvoice.DataProcessing
{
    public class VietnamRegionLicense
    {
        public List<VietnamRegionLicenseReport> GetData(string filePath)
        {
            var setting = RegistryHelper.GetObject(RegistryKey.VietnamLicenseColumnRowSetting,
                new VietnamLicenseColumnRowSetting());

            var xlApp = new Excel.Application();
            var xlWorkbook = xlApp.Workbooks.Open(filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[setting.SheetIndex];
            var xlRange = xlWorksheet.UsedRange;

            var rowCount = xlRange.Rows.Count;
            // var colCount = xlRange.Columns.Count;

            var result = new List<VietnamRegionLicenseReport>();

            //iterate over the rows and columns and print to the console as it appears in the file
            //excel is not zero based!!
            for (var i = setting.StartRowIndex; i <= rowCount; i++)
            {
                result.Add(new VietnamRegionLicenseReport()
                {
                    SchoolName = xlRange.Cells[i, setting.SchoolColumn]?.Value2?.ToString(),
                    CampusName = xlRange.Cells[i, setting.CampusColumn]?.Value2?.ToString(),
                    ClassName = xlRange.Cells[i, setting.ClassColumn]?.Value2?.ToString(),
                    UnitNumber = xlRange.Cells[i, setting.CurrentUnitColumn]?.Value2?.ToString(),
                    TotalAdjustment = xlRange.Cells[i, setting.TotalAdjustment]?.Value2?.ToString(),
                    TotalBilled = xlRange.Cells[i, setting.TotalBilledColumn]?.Value2?.ToString(),
                    LittleSEEDLicense = xlRange.Cells[i, setting.LittleSeedColumn]?.Value2?.ToString()
                });
            }

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return result;
        }
    }
}
