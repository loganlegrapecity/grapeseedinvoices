﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using GrapeSeedInvoice.DataProcessing.Models;
using GrapeSeedInvoice.DataProcessing.Models.FilterParameters;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice.DataProcessing
{
    /// <summary>
    /// Class is used to fetch all data from GrapeSeed api
    /// </summary>
    public class GrapeSeedCommonApi
    {
        private bool IsTestEnvironment { get; set; }
        private ApiHelper ApiHelper { get; set; }
        private ApiLink ApiLink { get; set; }

        public GrapeSeedCommonApi(bool isTestEnvironment, string userName, string password)
        {
            this.IsTestEnvironment = isTestEnvironment;
            this.ApiHelper = new ApiHelper(isTestEnvironment, userName, password);
            this.ApiLink = new ApiLink(isTestEnvironment);
        }

        public List<AccountDto> GetAccountsBySchool(string schoolId)
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetAccountsBySchool)} schoolId = {schoolId}");
            var accountIds = GetAccountIdsOfSchool(schoolId);
            if (accountIds == null || accountIds?.Count == 0)
            {
                return null;
            }

            return GetAccounts(accountIds);
        }

        public List<AccountDto> GetAccounts(List<string> accountIds)
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetAccounts)} total accounts = {accountIds.Count}");
            return this.ApiHelper.PostApi<List<AccountDto>>(ApiLink.UserApi, new UserRequestDto()
            {
                UserIds = accountIds
            }.ToJsonString());
        }

        public SchoolListDto GetSchools()
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetSchools)}");
            var dataPath = FolderHelper.CreateCurrentSubfolder("Data");

            var filePath = IsTestEnvironment ? $"{dataPath}\\Schools_TEST.txt" : $"{dataPath}\\Schools.txt";

            var schools = this.ApiHelper.GetApi<SchoolListDto>(ApiLink.SchoolApi, filePath);
            return schools;
        }

        public List<string> GetAccountIdsOfSchool(string schoolId)
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetAccountIdsOfSchool)} schoolId = {schoolId}");
            var api = string.Format(ApiLink.AccountManagersOfSchoolApi, schoolId);
            return this.ApiHelper.GetApi<List<string>>(api);
        }

        /// <summary>
        /// Lấy sản phẩm từ đơn đã được đặt hàng
        /// </summary>
        /// <param name="submissionDate"></param>
        /// <returns></returns>
        public List<MaterialProductDto> GetMaterialProducts(DateTime submissionDate)
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetMaterialProducts)}");
            bool.TryParse(RegistryHelper.GetString(RegistryKey.ExcludeMaterialProducts), out var excludeMaterialProducts);
            if (excludeMaterialProducts)
            {
                return new List<MaterialProductDto>();
            }
            var data = this.ApiHelper.GetApi<List<MaterialProductDto>>(ApiLink.MaterialOrders);
            return data.Where(x => x.submissionDate >= submissionDate.Date).ToList();
        }

        public List<ProductDto> GetProducts()
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetProducts)}");
            return this.ApiHelper.GetApi<List<ProductDto>>(ApiLink.Products);
        }

        public ProductDetailDto GetProductDetail(string productId)
        {
            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetProductDetail)} productId = {productId}");
            return this.ApiHelper.GetApi<ProductDetailDto>(string.Format(ApiLink.ProductPrice, productId));
        }

        public UserProfileDto GetUserProfile(string email)
        {
            var api = string.Format(ApiLink.UserProfile, email?.Trim());
            return this.ApiHelper.GetApi<List<UserProfileDto>>(api)?.FirstOrDefault();
        }

        public List<CampusDto> GetCampuses(string schoolId)
        {
            Guid.TryParse(schoolId, out var schoolIdGuid);
            if (schoolIdGuid == Guid.Empty)
            {
                return new List<CampusDto>();
            }

            LogHelper.LogMessage($"{nameof(GrapeSeedCommonApi)}.{nameof(GetCampuses)} schoolId = {schoolId}");
            var jsonBody = new CampusRequestDto(schoolIdGuid).ToJsonString();
            return this.ApiHelper.PostApi<List<CampusDto>>(ApiLink.CampusBySchool, jsonBody);
        }

        public MaterialOrderDetailDto GetMaterialDetail(string materialId)
        {
            return this.ApiHelper.GetApi<MaterialOrderDetailDto>(string.Format(ApiLink.MaterialOrder, materialId));
        }
    }
}