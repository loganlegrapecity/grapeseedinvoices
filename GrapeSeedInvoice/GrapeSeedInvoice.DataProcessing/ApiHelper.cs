﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using GrapeSeedInvoice.DataProcessing.Models;
using Newtonsoft.Json;

namespace GrapeSeedInvoice.DataProcessing
{
    public class ApiHelper
    {
        private AccessToken AccessToken { get; set; }
        private bool isTestEnvironment { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public ApiHelper(bool isTestEnv, string userName, string password)
        {
            this.isTestEnvironment = isTestEnv;
            this.Password = password;
            this.UserName = userName;
        }

        private enum ApiType
        {
            Post,
            Get,
            Put,
            Delete
        }

        public string GetToken()
        {
            // var userName = "malone.dunlavy@grapeseed.com";
            // var passWord = "Ma10n3..";
            // if (this.isTestEnvironment)
            // {
            //     userName = "sadmin@grapecity.com";
            //     passWord = "1qaz!QAZ";
            // }

            if (string.IsNullOrWhiteSpace(this.UserName) || string.IsNullOrWhiteSpace(this.Password))
            {
                throw new Exception("Username & password is required");
            }

            if (!string.IsNullOrWhiteSpace(AccessToken?.Token) && AccessToken?.ExpireDate > DateTime.Now.AddMinutes(1))
            {
                return AccessToken.Token;
            }

            using (var client = new HttpClient())
            {
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("scope", "offline_access basicinfo openid"),
                    new KeyValuePair<string, string>("username", this.UserName),
                    new KeyValuePair<string, string>("password", this.Password)
                });


                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Add("Content", "application/x-www-form-urlencoded");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //"Basic Z3JhcGVzZWVkLmdlbmVyYWw6RjAwOEM5OUY1MDFDNDg3M0E4RDczNjIxQUI4OEYxMEU="
                const string basicToken = "Basic Z3N2OkYxMThDOTlGNTAxQzQ4NzNBOEQ3MzYyMUFCODhGMTBF";
                client.DefaultRequestHeaders.Add("Authorization", basicToken);
                var res = client.PostAsync(new ApiLink(isTestEnvironment).TokenApi, formContent).Result;

                if (res != null && res.IsSuccessStatusCode)
                {
                    var response = res.Content.ReadAsStringAsync().Result;
                    var token = JsonConvert.DeserializeObject<TokenApiDto>(response);

                    this.AccessToken = new AccessToken()
                    {
                        Token = token.access_token,
                        ExpireDate = DateTime.Now.AddSeconds(token.expires_in)
                    };

                    return token?.access_token;
                }

                return null;
            }
        }

        public T GetApi<T>(string urlApi, string saveResponseDataFilePath = null)
        {
            return this.CallApi<T>(urlApi, ApiType.Get, "", saveResponseDataFilePath);
        }

        public T PostApi<T>(string urlApi, string bodyJson, string saveResponseDataFilePath = null)
        {
            return this.CallApi<T>(urlApi, ApiType.Post, bodyJson, saveResponseDataFilePath);
        }

        private TOutputModel CallApi<TOutputModel>(string apiUrl, ApiType apiType, string bodyJson = "",
            string saveResponseDataFilePath = null)
        {
            if (string.IsNullOrEmpty(apiUrl))
            {
                throw new ArgumentNullException(nameof(apiUrl));
            }

            var token = GetToken();
            //use to pass SSL
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls12 |
                SecurityProtocolType.Tls11 |
                SecurityProtocolType.Tls;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.BaseAddress = new Uri(apiUrl);

                //use to pass SSL
                client.DefaultRequestHeaders.Add("User-Agent", "Invoices App");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (!string.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                }

                HttpResponseMessage res = null;
                StringContent content = null;
                if (apiType == ApiType.Post || apiType == ApiType.Put)
                {
                    content = new StringContent(bodyJson, Encoding.UTF8, "application/json");
                }

                switch (apiType)
                {
                    case ApiType.Post:
                        res = client.PostAsync(apiUrl, content).Result;
                        break;
                    case ApiType.Get:
                        res = client.GetAsync(apiUrl).Result;
                        break;
                    case ApiType.Delete:
                        res = client.DeleteAsync(apiUrl).Result;
                        break;
                    case ApiType.Put:
                        res = client.PutAsync(apiUrl, content).Result;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(apiType), apiType, null);
                }

                if (res != null && res.IsSuccessStatusCode)
                {
                    var response = res.Content.ReadAsStringAsync().Result;
                    if (!string.IsNullOrWhiteSpace(saveResponseDataFilePath))
                    {
                        File.WriteAllText(saveResponseDataFilePath, response);
                    }

                    return JsonConvert.DeserializeObject<TOutputModel>(response);
                }
                else if (res != null)
                {
                    throw new Exception(res.ReasonPhrase);
                }

                throw new Exception("Result is null");
            }
        }
    }

    public class AccessToken
    {
        public string Token { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}