﻿namespace GrapeSeedInvoice.DataProcessing
{
    public class ApiLink
    {
        private bool isTestEnv = false;
        private string serviceApi { get; set; }
        private string accountApi { get; set; }
        private string VnRegionId { get; set; }

        public ApiLink(bool isTestEnvironment)
        {
            this.isTestEnv = isTestEnvironment;
            serviceApi = isTestEnvironment ? "https://glapi-test.grapecitydev.com" : "https://services.grapeseed.com";
            accountApi = isTestEnvironment ? "https://glas-test.grapecitydev.com" : "https://account.grapeseed.com";
            VnRegionId = isTestEnvironment
                ? "6e4ee24c-4081-4343-999a-e2847e095f7e" // VietNamRegion_Gina2_English
                : "49c384f1-8f63-40f4-8ff1-3e57d139c3d5"; // Vietnam region trên production
        }

        public string TokenApi => $"{accountApi}/connect/token";
        public string SchoolApi => $"{serviceApi}/admin/v1/schools?offset=0&disabled=false&" +
                                   $"regionId={VnRegionId}&sortBy=name&isDescending=false";

        //Updated new API
        public string UserApi => $"{serviceApi}/admin/v1/app/regions/{VnRegionId}/users"; // $"{serviceApi}/account/v1/users";
        
        public string UserProfile => $"{serviceApi}/account/v1/users/internal/search?email={{0}}";

        public string AccountManagersOfSchoolApi => $"{serviceApi}/admin/v1/schools/{{0}}/accountmanagers?";

        public string MaterialOrders => $"{serviceApi}/admin/v1/regions/{VnRegionId}/materialorders?status=3";
        public string MaterialOrder => $"{serviceApi}/admin/v1/materialorders/{{0}}"; //{0} là material order
        
        public string Products => $"{serviceApi}/admin/v1/products/local?" +
                                  $"offset=0&limit=2000&disabled=false&filterText=vietnam&" +
                                  $"sortBy=regionEnglishName&isDescending=false&1584346635478";

        public string ProductPrice => $"{serviceApi}/admin/v1/products/local/{{0}}";
        
        // updated to new API
        public string CampusBySchool => $"{serviceApi}/admin/v1/app/regions/{VnRegionId}/campuses";

        
    }
}