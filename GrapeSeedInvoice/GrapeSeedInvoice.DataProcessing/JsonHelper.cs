﻿using Newtonsoft.Json;

namespace GrapeSeedInvoice.DataProcessing
{
    public static class JsonHelper
    {
        public static string ToJsonString<T>(this T model)
        {
            return JsonConvert.SerializeObject(model);
        }
    }
}