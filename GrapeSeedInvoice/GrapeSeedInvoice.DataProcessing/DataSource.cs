﻿using GrapeSeedInvoice.Helper;
using GrapeSeedInvoice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using GrapeSeedInvoice.DataProcessing.Models;

namespace GrapeSeedInvoice.DataProcessing
{
    public class DataSource
    {
        private GrapeSeedCommonApi commonApiCaller;
        private List<CampusDto> allCampuses;

        public List<AccountManager> GetData(SettingForProcessingDto setting)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetData)}");
            commonApiCaller = new GrapeSeedCommonApi(setting.IsTestEnv, setting.UserName, setting.Password);

            // get all schools
            var schools = setting.SpecificSchools?.Count > 0 ? setting.SpecificSchools : commonApiCaller.GetSchools()?.Schools;
            if (schools == null || schools?.Count == 0)
                return null;

            var accountIds = new List<string>();

            allCampuses = GetAllCampuses(schools.Select(x => x.id).ToList());

            foreach (var school in schools)
            {
                school.AccountManagerIds = commonApiCaller.GetAccountIdsOfSchool(school.id);
                school.SchoolAddress = GetCampusAddress(school.id);
                // commonApiCaller.GetSchoolAddress(school.id);

                if (school.AccountManagerIds == null || !school.AccountManagerIds.Any())
                {
                    continue;
                }

                foreach (var accountId in school.AccountManagerIds.Where(accountId => accountIds.All(x => x != accountId)))
                {
                    accountIds.Add(accountId);
                }
            }

            return GetAccountManagers(setting, accountIds, schools);
        }

        private string GetCampusAddress(string schoolId)
        {
            var campuses = allCampuses.Where(x => x.SchoolId == schoolId).ToList();
            var primary = campuses.FirstOrDefault(x => x.IsPrimary);
            var address = primary?.AddressLine1 ?? primary?.AddressLine2;
            if (string.IsNullOrWhiteSpace(address))
            {
                var fCampus = campuses.FirstOrDefault(x =>
                    !string.IsNullOrWhiteSpace(x.AddressLine1) || !string.IsNullOrWhiteSpace(x.AddressLine2));
                address = fCampus?.AddressLine1 ?? fCampus?.AddressLine2;
            }

            return address;
        }

        private List<CampusDto> GetAllCampuses(List<string> schoolIds)
        {
            var rs = new List<CampusDto>();
            if (schoolIds == null || !schoolIds.Any())
            {
                return rs;
            }

            // chỗ này có thể tối ưu bằng 1 API truyền vào 1 mảng các schoolIds
            // hiện chưa có API nào như thế
            foreach (var schoolId in schoolIds)
            {
                var campus = commonApiCaller.GetCampuses(schoolId);
                if (campus?.Any() == true)
                {
                    rs.AddRange(campus);
                }
            }

            return rs;
        }

        private List<AccountManager> GetAccountManagers(SettingForProcessingDto setting, List<string> accountIds, List<SchoolDto> schools)
        {
            // get all material products
            var orderedMaterialProducts = commonApiCaller.GetMaterialProducts(setting.StartShippedDate.AddDays(-40));

            // get products
            var productsListInDatabase = orderedMaterialProducts.Count > 0 ? commonApiCaller.GetProducts() : new List<ProductDto>();

            // all schools in license file
            var licenseData = new VietnamRegionLicense().GetData(setting.LicenseExcelFile);

            var data = new List<AccountManager>();
            var accountManagers = commonApiCaller.GetAccounts(accountIds);
            var ignoreAccountMangers = GetIgnoreAccountManagers();
            foreach (var account in accountManagers)
            {
                if (ignoreAccountMangers.Any(x => x.Equals(account.Email, StringComparison.CurrentCultureIgnoreCase) ||
                                                  x.Equals(account.Name, StringComparison.CurrentCultureIgnoreCase)))
                {
                    continue;
                }

                var accountManager = new AccountManager
                {
                    Id = account.UserId,
                    Email = account.Email,
                    Name = account.Name,
                    Schools = GetSchoolsByAccountManager(setting, schools, account, orderedMaterialProducts, productsListInDatabase, licenseData)
                };

                // Các trường học của Account Manager
                if (accountManager.Schools.Any())
                {
                    data.Add(accountManager);
                }
            }

            return data;
        }

        private List<School> GetSchoolsByAccountManager(SettingForProcessingDto setting, List<SchoolDto> schools, AccountDto account,
            List<MaterialProductDto> orderedMaterialProducts, List<ProductDto> productsListInDatabase,
            List<VietnamRegionLicenseReport> licenseData)
        {
            var schoolsOfAccountManager = schools.Where(x => x.AccountManagerIds.Contains(account.UserId))
                .Select(x => new School()
                {
                    Name = x.name,
                    Address = x.SchoolAddress,
                    InvoiceData = new InvoiceData()
                    {
                        Address = x.SchoolAddress,
                        InvoiceDate = DateTime.Now.Day + $" Tháng {DateTime.Now.Month} Năm {DateTime.Now.Year}",
                        SchoolName = x.name,
                        Products = GetProducts(x.id, orderedMaterialProducts, productsListInDatabase, licenseData, x.name,
                            setting.StartShippedDate,
                            setting.EndShippedDate),
                        UsdExchangeRate = setting.UsdExchangeRate
                    }
                }).ToList();
            return schoolsOfAccountManager.Where(x => x.InvoiceData.Products?.Count > 0).ToList();;
        }

        public List<InvoiceProduct> GetProducts(string schoolId,
            List<MaterialProductDto> orderedMaterialProducts,
            List<ProductDto> productsListInDatabase,
            List<VietnamRegionLicenseReport> licenseData,
            string schoolName, DateTime startShippedDate, DateTime endShippedDate)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetProducts)}");
            var invoiceProducts = new List<InvoiceProduct>();

            // get products in license file (lấy ở file excel)
            invoiceProducts.AddRange(GetProductsFromDataLicense(licenseData, schoolId, schoolName));

            if (orderedMaterialProducts?.Count > 0)
            {
                invoiceProducts.AddRange(GetProductsFromMaterial(schoolId, orderedMaterialProducts, productsListInDatabase,
                    startShippedDate, endShippedDate));
            }

            return invoiceProducts;
        }

        private List<InvoiceProduct> GetProductsFromDataLicense(List<VietnamRegionLicenseReport> licenseData, string schoolId,
            string schoolName)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetProductsFromDataLicense)}");
            if (licenseData == null || licenseData.Count == 0 || string.IsNullOrWhiteSpace(schoolName))
                return new List<InvoiceProduct>();

            var invoiceProducts = new List<InvoiceProduct>();
            var campusesInLicenseFile = licenseData.Where(x => x.SchoolName?.ToLower()?.RemoveVietnameseSign() ==
                                                               schoolName.ToLower().RemoveVietnameseSign()).ToList();
            var (grapeSeedPrice, littleSeedPrice) = GetGrapeSeedPrice();

            // var campuses = commonApiCaller.GetCampuses(schoolId);

            foreach (var campus in campusesInLicenseFile)
            {
                int.TryParse(campus.TotalBilled, out var totalBilled);
                if (totalBilled <= 0)
                {
                    continue;
                }

                float.TryParse(campus.TotalAdjustment, out var adjustment);
                var isLittleSeed = campus.LittleSEEDLicense?.Trim() != "0";
                var version = isLittleSeed ? "LittleSEED" : "GrapeSEED";
                var totalLicenseMonths = GetLicenseMonths(campus.CampusName);
                invoiceProducts.Add(new InvoiceProduct()
                {
                    // totalBilled được lấy từ file excel
                    // file excel này được xuất ra từ phần mềm, mà phần mềm thì phần tính quantity đã bao gồm adjustment rồi (ko sửa được logic phần mềm)
                    // do đó để tính quantity chuẩn thì phải trừ đi phần đã adjustment
                    Quantity = totalBilled - (int) adjustment, //totalBilled
                    Adjustment = adjustment,
                    ProductName = $"{campus.CampusName} {campus.ClassName} - Unit {campus.UnitNumber} {version}",
                    Price = (float) Math.Round(isLittleSeed ? littleSeedPrice / totalLicenseMonths : grapeSeedPrice / totalLicenseMonths, 2,
                        MidpointRounding.ToEven),
                });
            }

            return invoiceProducts;
        }

        private static (float, float) GetGrapeSeedPrice()
        {
            float.TryParse(RegistryHelper.GetString(RegistryKey.GrapeSeedPrice), out var grapeSeedPrice);
            if (grapeSeedPrice <= 0)
            {
                grapeSeedPrice = DefaultConst.GrapeSeedPrice;
            }

            float.TryParse(RegistryHelper.GetString(RegistryKey.LittleSeedPrice), out var littleSeedPrice);
            if (littleSeedPrice <= 0)
            {
                littleSeedPrice = DefaultConst.LittleSeedPrice;
            }

            return (grapeSeedPrice, littleSeedPrice);
        }

        private List<InvoiceProduct> GetProductsFromMaterial(string schoolId, List<MaterialProductDto> materialProduct,
            List<ProductDto> products, DateTime startShippedDate, DateTime endShippedDate)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetProductsFromMaterial)}");
            var invoiceProducts = new List<InvoiceProduct>();
            if (string.IsNullOrWhiteSpace(schoolId) || materialProduct == null || materialProduct.Count == 0)
                return invoiceProducts;

            const int shippedStatus = 3; //3 là sản phẩm đã ở trạng thái
            foreach (var item in materialProduct.Where(x => x.schoolId == schoolId && x.status == shippedStatus))
            {
                if (!this.IsValidShippedDate(item.id, startShippedDate, endShippedDate))
                {
                    continue;
                }

                foreach (var classMaterial in item.schoolClassMaterialOrderProduct)
                {
                    foreach (var product in classMaterial.materialOrderProduct)
                    {
                        //TODO: Set sẵn productType =2 , nếu nó = 2 thì lấy, khác 2 thì bỏ qua sản phẩm này
                        if (product.productType != 2)
                            continue;

                        invoiceProducts.Add(new InvoiceProduct()
                        {
                            ProductName =
                                $"{item.campusName} {classMaterial.schoolClassName} - Unit {product.productUnit} - {product.productName}",
                            Price = (float) GetMaterialPrice(product, products),
                            Quantity = product.quantity,
                            //  TotalPrice = 1 //TODO: Excel has the formula to do this
                        });
                    }
                }
            }

            return invoiceProducts;
        }

        private bool IsValidShippedDate(string materialId, DateTime startShippedDate, DateTime endShippedDate)
        {
            var shippedDate = commonApiCaller.GetMaterialDetail(materialId)?.ShippedDate;
            if (shippedDate == null)
            {
                return false;
            }

            return shippedDate.Value.Date >= startShippedDate.Date && shippedDate.Value.Date < endShippedDate.Date;
        }

        private List<ProductDetailDto> productDetails = new List<ProductDetailDto>();

        private double GetMaterialPrice(MaterialOrderProduct product, List<ProductDto> products)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetMaterialPrice)}");
            var productId = products.FirstOrDefault(x => x.name == product.productName &&
                                                         x.startUnit <= product.productUnit &&
                                                         product.productUnit <= x.endUnit)?.id;

            if (!string.IsNullOrWhiteSpace(productId))
            {
                var detail = productDetails.FirstOrDefault(p => p.id == productId);
                if (detail == null)
                {
                    detail = commonApiCaller.GetProductDetail(productId);
                    productDetails.Add(detail);
                }

                return detail.unitPrices.FirstOrDefault(x => x.startUnit <= product.productUnit && product.productUnit <= x.endUnit)
                    ?.price ?? 0;
            }

            return 0;
        }

        private List<string> GetIgnoreAccountManagers()
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetIgnoreAccountManagers)}");
            var rs = new List<string>() {"zGSVN Admin"};
            var accounts = RegistryHelper.GetString(RegistryKey.IgnoreAccountManagers);
            if (string.IsNullOrWhiteSpace(accounts))
                return rs;

            var settings = accounts.Replace(";", ",")
                .Split(',')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Trim())
                .ToList();

            rs.AddRange(settings);
            return rs;
        }

        /// <summary>
        /// Lấy số tháng sử dụng bản quyền của 1 campus
        /// </summary>
        /// <param name="campusName"></param>
        /// <returns></returns>
        private int GetLicenseMonths(string campusName)
        {
            LogHelper.LogMessage($"{nameof(DataSource)}.{nameof(GetLicenseMonths)} campusName = {campusName}");
            if (allCampuses == null || allCampuses.Count == 0 || string.IsNullOrWhiteSpace(campusName))
            {
                return 12;
            }

            var campusPlace =
                allCampuses.FirstOrDefault(x => string.Equals(x.Name.RemoveVietnameseSign(), campusName.RemoveVietnameseSign(),
                    StringComparison.CurrentCultureIgnoreCase));

            int.TryParse(campusPlace?.CampusNumber, out var campusGsNumber);
            return campusGsNumber > 0 ? campusGsNumber : 12;
        }
    }
}