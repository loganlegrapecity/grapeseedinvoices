﻿using GrapeSeedInvoice.Helper;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace GrapeSeedInvoice
{
    public partial class FrmPriceSetting : Form
    {
        public FrmPriceSetting()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            this.LoadSetting();
        }

        public sealed override Size MinimumSize
        {
            get => base.MinimumSize;
            set => base.MinimumSize = value;
        }

        public sealed override Size MaximumSize
        {
            get => base.MaximumSize;
            set => base.MaximumSize = value;
        }

        private void btnSaveSetting_Click(object sender, EventArgs e)
        {
            SaveSetting();
            this.Dispose();
        }

        private void SaveSetting()
        {
            float.TryParse(txtGrapeSeedPrice.Text, out float grapeSeedPrice);
            float.TryParse(txtLittleSeedPrice.Text, out float littleSeedPrice);

            RegistryHelper.SetValue(RegistryKey.GrapeSeedPrice, grapeSeedPrice.ToString(CultureInfo.InvariantCulture));
            RegistryHelper.SetValue(RegistryKey.LittleSeedPrice, littleSeedPrice.ToString(CultureInfo.InvariantCulture));
            RegistryHelper.SetValue(RegistryKey.ExcludeMaterialProducts, chkExcludeMaterialProducts.Checked.ToString(CultureInfo.InvariantCulture));
        }

        private void LoadSetting()
        {
            float.TryParse(RegistryHelper.GetString(RegistryKey.GrapeSeedPrice), out float grapeSeedPrice);
            float.TryParse(RegistryHelper.GetString(RegistryKey.LittleSeedPrice), out float littleSeedPrice);
            if (grapeSeedPrice > 0)
            {
                txtGrapeSeedPrice.Text = grapeSeedPrice + "";
            }
            else
            {
                txtGrapeSeedPrice.Text = DefaultConst.GrapeSeedPrice+"";
            }

            if (littleSeedPrice > 0)
            {
                txtLittleSeedPrice.Text = littleSeedPrice + "";
            }
            else
            {
                txtLittleSeedPrice.Text = DefaultConst.LittleSeedPrice+"";
            }

            bool.TryParse(RegistryHelper.GetString(RegistryKey.ExcludeMaterialProducts), out bool excludeMaterialProducts);
            chkExcludeMaterialProducts.Checked = excludeMaterialProducts;
        }
    }
}
