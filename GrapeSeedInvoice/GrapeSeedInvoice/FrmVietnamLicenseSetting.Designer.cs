﻿namespace GrapeSeedInvoice
{
    partial class FrmVietnamLicenseSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSchoolName = new System.Windows.Forms.TextBox();
            this.txtCampus = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCurrentUnit = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotalBilled = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLittleSeedLicense = new System.Windows.Forms.TextBox();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtStartRowIndex = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSheetIndex = new System.Windows.Forms.TextBox();
            this.txtTotalAdjustment = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "School";
            // 
            // txtSchoolName
            // 
            this.txtSchoolName.Location = new System.Drawing.Point(117, 18);
            this.txtSchoolName.MaxLength = 1;
            this.txtSchoolName.Name = "txtSchoolName";
            this.txtSchoolName.Size = new System.Drawing.Size(100, 20);
            this.txtSchoolName.TabIndex = 1;
            // 
            // txtCampus
            // 
            this.txtCampus.Location = new System.Drawing.Point(117, 49);
            this.txtCampus.MaxLength = 1;
            this.txtCampus.Name = "txtCampus";
            this.txtCampus.Size = new System.Drawing.Size(100, 20);
            this.txtCampus.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Campus";
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(117, 78);
            this.txtClass.MaxLength = 1;
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(100, 20);
            this.txtClass.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Class";
            // 
            // txtCurrentUnit
            // 
            this.txtCurrentUnit.Location = new System.Drawing.Point(355, 18);
            this.txtCurrentUnit.MaxLength = 1;
            this.txtCurrentUnit.Name = "txtCurrentUnit";
            this.txtCurrentUnit.Size = new System.Drawing.Size(100, 20);
            this.txtCurrentUnit.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(248, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Current unit";
            // 
            // txtTotalBilled
            // 
            this.txtTotalBilled.Location = new System.Drawing.Point(355, 48);
            this.txtTotalBilled.MaxLength = 1;
            this.txtTotalBilled.Name = "txtTotalBilled";
            this.txtTotalBilled.Size = new System.Drawing.Size(100, 20);
            this.txtTotalBilled.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(248, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total billed";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "LittleSEED License";
            // 
            // txtLittleSeedLicense
            // 
            this.txtLittleSeedLicense.Location = new System.Drawing.Point(355, 78);
            this.txtLittleSeedLicense.MaxLength = 1;
            this.txtLittleSeedLicense.Name = "txtLittleSeedLicense";
            this.txtLittleSeedLicense.Size = new System.Drawing.Size(100, 20);
            this.txtLittleSeedLicense.TabIndex = 11;
            // 
            // btnSaveData
            // 
            this.btnSaveData.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSaveData.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSaveData.Location = new System.Drawing.Point(255, 173);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(95, 35);
            this.btnSaveData.TabIndex = 12;
            this.btnSaveData.Text = "Save";
            this.btnSaveData.UseVisualStyleBackColor = false;
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.GrayText;
            this.btnReset.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReset.Location = new System.Drawing.Point(127, 173);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(122, 35);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "Reset default setting";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Start data row index";
            // 
            // txtStartRowIndex
            // 
            this.txtStartRowIndex.Location = new System.Drawing.Point(117, 137);
            this.txtStartRowIndex.MaxLength = 4;
            this.txtStartRowIndex.Name = "txtStartRowIndex";
            this.txtStartRowIndex.Size = new System.Drawing.Size(100, 20);
            this.txtStartRowIndex.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(250, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Sheet index";
            // 
            // txtSheetIndex
            // 
            this.txtSheetIndex.Location = new System.Drawing.Point(355, 108);
            this.txtSheetIndex.MaxLength = 1;
            this.txtSheetIndex.Name = "txtSheetIndex";
            this.txtSheetIndex.Size = new System.Drawing.Size(100, 20);
            this.txtSheetIndex.TabIndex = 17;
            // 
            // txtTotalAdjustment
            // 
            this.txtTotalAdjustment.Location = new System.Drawing.Point(117, 107);
            this.txtTotalAdjustment.MaxLength = 1;
            this.txtTotalAdjustment.Name = "txtTotalAdjustment";
            this.txtTotalAdjustment.Size = new System.Drawing.Size(100, 20);
            this.txtTotalAdjustment.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Total adjustments";
            // 
            // FrmVietnamLicenseSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 223);
            this.Controls.Add(this.txtTotalAdjustment);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSheetIndex);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtStartRowIndex);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSaveData);
            this.Controls.Add(this.txtLittleSeedLicense);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTotalBilled);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCurrentUnit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtClass);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCampus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSchoolName);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmVietnamLicenseSetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vietnam license form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSchoolName;
        private System.Windows.Forms.TextBox txtCampus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCurrentUnit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotalBilled;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLittleSeedLicense;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtStartRowIndex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSheetIndex;
        private System.Windows.Forms.TextBox txtTotalAdjustment;
        private System.Windows.Forms.Label label9;
    }
}