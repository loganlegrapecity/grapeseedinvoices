﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice
{
    public partial class FrmFinalResult : Form
    {
        public FrmFinalResult()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
        }

        public sealed override Size MinimumSize
        {
            get => base.MinimumSize;
            set => base.MinimumSize = value;
        }

        public sealed override Size MaximumSize
        {
            get => base.MaximumSize;
            set => base.MaximumSize = value;
        }

        public void BindingResultData(string totalMinutes, int totalInvoices, string outputPath)
        {
            txtOutputPath.Text = outputPath;
            lblTotalTime.Text +=  $" {totalMinutes} minute(s)";
            lblTotalInvoices.Text += totalInvoices == 1 ? $" {totalInvoices} invoice" : $" {totalInvoices} invoices";
            // this.ShowDialog();
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            var folderPath = txtOutputPath.Text;
            if (Directory.Exists(folderPath))
            {
                var startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe",
                    WindowStyle = ProcessWindowStyle.Maximized
                };

                Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show($"{folderPath} Directory does not exist!");
            }
        }
    }
}
