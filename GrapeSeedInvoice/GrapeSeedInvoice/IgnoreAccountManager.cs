﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice
{
    public partial class IgnoreAccountManager : Form
    {
        public IgnoreAccountManager()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            LoadSetting();
        }

        public sealed override Size MinimumSize
        {
            get => base.MinimumSize;
            set => base.MinimumSize = value;
        }

        public sealed override Size MaximumSize
        {
            get => base.MaximumSize;
            set => base.MaximumSize = value;
        }

        private void btnSaveIgnoreAccountManagers_Click(object sender, EventArgs e)
        {
            RegistryHelper.SetValue(RegistryKey.IgnoreAccountManagers, txtIgnoreAccountManagers.Text.Trim());
            this.Dispose();
        }

        private void LoadSetting()
        {
            var accounts = RegistryHelper.GetString(RegistryKey.IgnoreAccountManagers);
            if (string.IsNullOrWhiteSpace(accounts))
                accounts = DefaultConst.IgnoreAccountMangers;

            txtIgnoreAccountManagers.Text = accounts;
        }
    }
}
