﻿namespace GrapeSeedInvoice
{
    partial class FrmPriceSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPriceSetting));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLittleSeedPrice = new System.Windows.Forms.TextBox();
            this.txtGrapeSeedPrice = new System.Windows.Forms.TextBox();
            this.btnSaveSetting = new System.Windows.Forms.Button();
            this.chkExcludeMaterialProducts = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "LittleSEED license price/year";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "GrapeSEED license price/year";
            // 
            // txtLittleSeedPrice
            // 
            this.txtLittleSeedPrice.Location = new System.Drawing.Point(200, 26);
            this.txtLittleSeedPrice.Name = "txtLittleSeedPrice";
            this.txtLittleSeedPrice.Size = new System.Drawing.Size(100, 20);
            this.txtLittleSeedPrice.TabIndex = 2;
            // 
            // txtGrapeSeedPrice
            // 
            this.txtGrapeSeedPrice.Location = new System.Drawing.Point(200, 59);
            this.txtGrapeSeedPrice.Name = "txtGrapeSeedPrice";
            this.txtGrapeSeedPrice.Size = new System.Drawing.Size(100, 20);
            this.txtGrapeSeedPrice.TabIndex = 3;
            // 
            // btnSaveSetting
            // 
            this.btnSaveSetting.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSaveSetting.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSaveSetting.Location = new System.Drawing.Point(184, 141);
            this.btnSaveSetting.Name = "btnSaveSetting";
            this.btnSaveSetting.Size = new System.Drawing.Size(95, 35);
            this.btnSaveSetting.TabIndex = 4;
            this.btnSaveSetting.Text = "Save";
            this.btnSaveSetting.UseVisualStyleBackColor = false;
            this.btnSaveSetting.Click += new System.EventHandler(this.btnSaveSetting_Click);
            // 
            // chkExcludeMaterialProducts
            // 
            this.chkExcludeMaterialProducts.AutoSize = true;
            this.chkExcludeMaterialProducts.Location = new System.Drawing.Point(34, 100);
            this.chkExcludeMaterialProducts.Name = "chkExcludeMaterialProducts";
            this.chkExcludeMaterialProducts.Size = new System.Drawing.Size(210, 17);
            this.chkExcludeMaterialProducts.TabIndex = 5;
            this.chkExcludeMaterialProducts.Text = "Exclude material products from invoice.";
            this.chkExcludeMaterialProducts.UseVisualStyleBackColor = true;
            // 
            // FrmPriceSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 190);
            this.Controls.Add(this.chkExcludeMaterialProducts);
            this.Controls.Add(this.btnSaveSetting);
            this.Controls.Add(this.txtGrapeSeedPrice);
            this.Controls.Add(this.txtLittleSeedPrice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPriceSetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Invoice Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLittleSeedPrice;
        private System.Windows.Forms.TextBox txtGrapeSeedPrice;
        private System.Windows.Forms.Button btnSaveSetting;
        private System.Windows.Forms.CheckBox chkExcludeMaterialProducts;
    }
}