﻿using GrapeSeedInvoice.Helper;
using GrapeSeedInvoice.Models;
using System;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace GrapeSeedInvoice
{
    public partial class FrmVietnamLicenseSetting : Form
    {
        public FrmVietnamLicenseSetting()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            LoadSetting();
        }

        public sealed override Size MinimumSize
        {
            get => base.MinimumSize;
            set => base.MinimumSize = value;
        }

        public sealed override Size MaximumSize
        {
            get => base.MaximumSize;
            set => base.MaximumSize = value;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                BindSettingToForm(new VietnamLicenseColumnRowSetting());
                SaveSetting();
            }
            catch (Exception exception)
            {
                LogHelper.LogMessage("FrmVietnamLicenseSetting.btnReset_Click " + exception.Message);
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSetting();
                this.Dispose();
            }
            catch (Exception exception)
            {
                LogHelper.LogMessage("FrmVietnamLicenseSetting.btnSaveData_Click " + exception.Message);
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveSetting()
        {
            int.TryParse(txtStartRowIndex.Text, out int rowIndex);
            int.TryParse(txtSheetIndex.Text, out int sheetIndex);
            var setting = new VietnamLicenseColumnRowSetting()
            {
                CampusColumn = txtCampus.Text,
                LittleSeedColumn = txtLittleSeedLicense.Text,
                SchoolColumn = txtSchoolName.Text,
                TotalBilledColumn = txtTotalBilled.Text,
                StartRowIndex = rowIndex,
                SheetIndex = sheetIndex,
                ClassColumn = txtClass.Text,
                CurrentUnitColumn = txtCurrentUnit.Text,
                TotalAdjustment = txtTotalAdjustment.Text
            };
            var str = JsonConvert.SerializeObject(setting);
            RegistryHelper.SetValue(RegistryKey.VietnamLicenseColumnRowSetting, str);
        }

        private void LoadSetting()
        {
            try
            {
                var setting = RegistryHelper.GetObject(RegistryKey.VietnamLicenseColumnRowSetting, new VietnamLicenseColumnRowSetting());
                BindSettingToForm(setting);

            }
            catch (Exception e)
            {
                LogHelper.LogMessage("FrmVietnamLicenseSetting.LoadSetting " + e.Message);
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindSettingToForm(VietnamLicenseColumnRowSetting setting)
        {
            txtStartRowIndex.Text = setting.StartRowIndex + "";
            txtCampus.Text = setting.CampusColumn;
            txtClass.Text = setting.ClassColumn;
            txtCurrentUnit.Text = setting.CurrentUnitColumn;
            txtLittleSeedLicense.Text = setting.LittleSeedColumn;
            txtSchoolName.Text = setting.SchoolColumn;
            txtSheetIndex.Text = setting.SheetIndex + "";
            txtTotalBilled.Text = setting.TotalBilledColumn;
            txtTotalAdjustment.Text = setting.TotalAdjustment;
        }
    }
}
