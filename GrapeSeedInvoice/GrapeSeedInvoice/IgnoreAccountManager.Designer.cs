﻿namespace GrapeSeedInvoice
{
    partial class IgnoreAccountManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IgnoreAccountManager));
            this.label1 = new System.Windows.Forms.Label();
            this.txtIgnoreAccountManagers = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSaveIgnoreAccountManagers = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(461, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter the email or name of account manager that you want to ignore for generating" +
    " the invoices  ";
            // 
            // txtIgnoreAccountManagers
            // 
            this.txtIgnoreAccountManagers.Location = new System.Drawing.Point(12, 79);
            this.txtIgnoreAccountManagers.Multiline = true;
            this.txtIgnoreAccountManagers.Name = "txtIgnoreAccountManagers";
            this.txtIgnoreAccountManagers.Size = new System.Drawing.Size(531, 115);
            this.txtIgnoreAccountManagers.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(280, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email or name are separated by comma (,) or semicolum (;)";
            // 
            // btnSaveIgnoreAccountManagers
            // 
            this.btnSaveIgnoreAccountManagers.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSaveIgnoreAccountManagers.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSaveIgnoreAccountManagers.Location = new System.Drawing.Point(236, 211);
            this.btnSaveIgnoreAccountManagers.Name = "btnSaveIgnoreAccountManagers";
            this.btnSaveIgnoreAccountManagers.Size = new System.Drawing.Size(95, 35);
            this.btnSaveIgnoreAccountManagers.TabIndex = 3;
            this.btnSaveIgnoreAccountManagers.Text = "Save";
            this.btnSaveIgnoreAccountManagers.UseVisualStyleBackColor = false;
            this.btnSaveIgnoreAccountManagers.Click += new System.EventHandler(this.btnSaveIgnoreAccountManagers_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(12, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(251, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "zGSVN Admin is always ignored even you set or not";
            // 
            // IgnoreAccountManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 263);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSaveIgnoreAccountManagers);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIgnoreAccountManagers);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IgnoreAccountManager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Excluded Account Managers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIgnoreAccountManagers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSaveIgnoreAccountManagers;
        private System.Windows.Forms.Label label3;
    }
}