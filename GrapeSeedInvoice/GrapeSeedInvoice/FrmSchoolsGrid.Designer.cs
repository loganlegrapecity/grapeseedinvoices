﻿namespace GrapeSeedInvoice
{
    partial class FrmSchoolsGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSchoolsGrid));
            this.grvSchools = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKeyword = new System.Windows.Forms.TextBox();
            this.btnSearchSchool = new System.Windows.Forms.Button();
            this.btnGetSelected = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.grvSelectedSchools = new System.Windows.Forms.DataGridView();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDeleteSelected = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblIndicator = new System.Windows.Forms.Label();
            this.btnPullData = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.grvSchools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.grvSelectedSchools)).BeginInit();
            this.SuspendLayout();
            // 
            // grvSchools
            // 
            this.grvSchools.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvSchools.Location = new System.Drawing.Point(12, 73);
            this.grvSchools.Name = "grvSchools";
            this.grvSchools.ReadOnly = true;
            this.grvSchools.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grvSchools.Size = new System.Drawing.Size(422, 289);
            this.grvSchools.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "School name or school Id";
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(146, 19);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Size = new System.Drawing.Size(288, 20);
            this.txtKeyword.TabIndex = 2;
            this.txtKeyword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKeyword_KeyDown);
            // 
            // btnSearchSchool
            // 
            this.btnSearchSchool.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSearchSchool.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSearchSchool.Location = new System.Drawing.Point(446, 18);
            this.btnSearchSchool.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearchSchool.Name = "btnSearchSchool";
            this.btnSearchSchool.Size = new System.Drawing.Size(75, 23);
            this.btnSearchSchool.TabIndex = 3;
            this.btnSearchSchool.Text = "Search";
            this.btnSearchSchool.UseVisualStyleBackColor = false;
            this.btnSearchSchool.Click += new System.EventHandler(this.btnSearchSchool_Click);
            // 
            // btnGetSelected
            // 
            this.btnGetSelected.Location = new System.Drawing.Point(451, 137);
            this.btnGetSelected.Name = "btnGetSelected";
            this.btnGetSelected.Size = new System.Drawing.Size(95, 23);
            this.btnGetSelected.TabIndex = 4;
            this.btnGetSelected.Text = "==>";
            this.btnGetSelected.UseVisualStyleBackColor = true;
            this.btnGetSelected.Click += new System.EventHandler(this.btnGetSelected_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (128)))), ((int) (((byte) (128)))));
            this.btnClear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnClear.Location = new System.Drawing.Point(524, 18);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // grvSelectedSchools
            // 
            this.grvSelectedSchools.AllowUserToAddRows = false;
            this.grvSelectedSchools.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvSelectedSchools.Location = new System.Drawing.Point(564, 73);
            this.grvSelectedSchools.Name = "grvSelectedSchools";
            this.grvSelectedSchools.ReadOnly = true;
            this.grvSelectedSchools.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grvSelectedSchools.ShowEditingIcon = false;
            this.grvSelectedSchools.Size = new System.Drawing.Size(419, 289);
            this.grvSelectedSchools.TabIndex = 6;
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAccept.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAccept.Location = new System.Drawing.Point(503, 385);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 35);
            this.btnAccept.TabIndex = 7;
            this.btnAccept.Text = "OK";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightCoral;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.Location = new System.Drawing.Point(419, 385);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 35);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Clear all";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDeleteSelected
            // 
            this.btnDeleteSelected.Location = new System.Drawing.Point(451, 175);
            this.btnDeleteSelected.Name = "btnDeleteSelected";
            this.btnDeleteSelected.Size = new System.Drawing.Size(93, 23);
            this.btnDeleteSelected.TabIndex = 9;
            this.btnDeleteSelected.Text = "<==";
            this.btnDeleteSelected.UseVisualStyleBackColor = true;
            this.btnDeleteSelected.Click += new System.EventHandler(this.btnDeleteSelected_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(562, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Selected schools for generating invoices";
            // 
            // lblIndicator
            // 
            this.lblIndicator.AutoSize = true;
            this.lblIndicator.Location = new System.Drawing.Point(12, 51);
            this.lblIndicator.Name = "lblIndicator";
            this.lblIndicator.Size = new System.Drawing.Size(57, 13);
            this.lblIndicator.TabIndex = 11;
            this.lblIndicator.Text = "All schools";
            // 
            // btnPullData
            // 
            this.btnPullData.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnPullData.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPullData.Location = new System.Drawing.Point(451, 73);
            this.btnPullData.Name = "btnPullData";
            this.btnPullData.Size = new System.Drawing.Size(93, 23);
            this.btnPullData.TabIndex = 12;
            this.btnPullData.Text = "Pull data";
            this.btnPullData.UseVisualStyleBackColor = false;
            this.btnPullData.Click += new System.EventHandler(this.btnPullData_Click);
            // 
            // FrmSchoolsGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 436);
            this.Controls.Add(this.btnPullData);
            this.Controls.Add(this.lblIndicator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDeleteSelected);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.grvSelectedSchools);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGetSelected);
            this.Controls.Add(this.btnSearchSchool);
            this.Controls.Add(this.txtKeyword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grvSchools);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSchoolsGrid";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Schools";
            ((System.ComponentModel.ISupportInitialize) (this.grvSchools)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.grvSelectedSchools)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.DataGridView grvSchools;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKeyword;
        private System.Windows.Forms.Button btnSearchSchool;
        private System.Windows.Forms.Button btnGetSelected;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridView grvSelectedSchools;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDeleteSelected;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblIndicator;
        private System.Windows.Forms.Button btnPullData;
    }
}