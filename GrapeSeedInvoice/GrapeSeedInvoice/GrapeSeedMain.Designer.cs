﻿namespace GrapeSeedInvoice
{
    partial class FrmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreateInvoice = new System.Windows.Forms.Button();
            this.txtInvoiceTemplatePath = new System.Windows.Forms.TextBox();
            this.btnOpenFileTemplate = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSettingForm = new System.Windows.Forms.ToolStripMenuItem();
            this.ignoreAccountManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turnOnTestTemplateModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vietnamRegionLicenseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsdRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVietnamRegionLicense = new System.Windows.Forms.TextBox();
            this.btnSelectLicenseFile = new System.Windows.Forms.Button();
            this.lblDownloadTemplateFile = new System.Windows.Forms.LinkLabel();
            this.pnBasicSetting = new System.Windows.Forms.GroupBox();
            this.dtpEndShippedDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartShippedDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSelectSchools = new System.Windows.Forms.Button();
            this.btnClearSpecificSchools = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSpecificSchools = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkZipfolder = new System.Windows.Forms.CheckBox();
            this.lblTurnOffTestMode = new System.Windows.Forms.LinkLabel();
            this.btnOpenInvoiceFolder = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.pnBasicSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Invoice template";
            // 
            // btnCreateInvoice
            // 
            this.btnCreateInvoice.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCreateInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnCreateInvoice.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCreateInvoice.Location = new System.Drawing.Point(383, 384);
            this.btnCreateInvoice.Name = "btnCreateInvoice";
            this.btnCreateInvoice.Size = new System.Drawing.Size(249, 45);
            this.btnCreateInvoice.TabIndex = 1;
            this.btnCreateInvoice.Text = "Generate Invoices";
            this.btnCreateInvoice.UseVisualStyleBackColor = false;
            this.btnCreateInvoice.Click += new System.EventHandler(this.btnCreateInvoice_Click);
            // 
            // txtInvoiceTemplatePath
            // 
            this.txtInvoiceTemplatePath.Location = new System.Drawing.Point(176, 39);
            this.txtInvoiceTemplatePath.Name = "txtInvoiceTemplatePath";
            this.txtInvoiceTemplatePath.Size = new System.Drawing.Size(502, 20);
            this.txtInvoiceTemplatePath.TabIndex = 2;
            // 
            // btnOpenFileTemplate
            // 
            this.btnOpenFileTemplate.Location = new System.Drawing.Point(684, 37);
            this.btnOpenFileTemplate.Name = "btnOpenFileTemplate";
            this.btnOpenFileTemplate.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFileTemplate.TabIndex = 3;
            this.btnOpenFileTemplate.Text = "...";
            this.btnOpenFileTemplate.UseVisualStyleBackColor = true;
            this.btnOpenFileTemplate.Click += new System.EventHandler(this.btnOpenFileTemplate_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.settingsToolStripMenuItem, this.logToolStripMenuItem, this.templateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(815, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.mnuSettingForm, this.ignoreAccountManagersToolStripMenuItem, this.turnOnTestTemplateModeToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.settingsToolStripMenuItem.Text = "Advanced Settings";
            // 
            // mnuSettingForm
            // 
            this.mnuSettingForm.Name = "mnuSettingForm";
            this.mnuSettingForm.Size = new System.Drawing.Size(262, 22);
            this.mnuSettingForm.Text = "Invoice Settings";
            this.mnuSettingForm.Click += new System.EventHandler(this.mnuSettingForm_Click);
            // 
            // ignoreAccountManagersToolStripMenuItem
            // 
            this.ignoreAccountManagersToolStripMenuItem.Name = "ignoreAccountManagersToolStripMenuItem";
            this.ignoreAccountManagersToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.ignoreAccountManagersToolStripMenuItem.Text = "Excluded Account Managers";
            this.ignoreAccountManagersToolStripMenuItem.Click += new System.EventHandler(this.ignoreAccountManagersToolStripMenuItem_Click);
            // 
            // turnOnTestTemplateModeToolStripMenuItem
            // 
            this.turnOnTestTemplateModeToolStripMenuItem.Name = "turnOnTestTemplateModeToolStripMenuItem";
            this.turnOnTestTemplateModeToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.turnOnTestTemplateModeToolStripMenuItem.Text = "Turn on test invoice template mode";
            this.turnOnTestTemplateModeToolStripMenuItem.Click += new System.EventHandler(this.turnOnTestTemplateModeToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.openLogFolderToolStripMenuItem});
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.logToolStripMenuItem.Text = "Log";
            // 
            // openLogFolderToolStripMenuItem
            // 
            this.openLogFolderToolStripMenuItem.Name = "openLogFolderToolStripMenuItem";
            this.openLogFolderToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.openLogFolderToolStripMenuItem.Text = "Open log folder";
            this.openLogFolderToolStripMenuItem.Click += new System.EventHandler(this.openLogFolderToolStripMenuItem_Click);
            // 
            // templateToolStripMenuItem
            // 
            this.templateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.invoiceTemplateToolStripMenuItem, this.vietnamRegionLicenseReportToolStripMenuItem});
            this.templateToolStripMenuItem.Name = "templateToolStripMenuItem";
            this.templateToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.templateToolStripMenuItem.Text = "Template";
            // 
            // invoiceTemplateToolStripMenuItem
            // 
            this.invoiceTemplateToolStripMenuItem.Name = "invoiceTemplateToolStripMenuItem";
            this.invoiceTemplateToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.invoiceTemplateToolStripMenuItem.Text = "Invoice template";
            this.invoiceTemplateToolStripMenuItem.Click += new System.EventHandler(this.invoiceTemplateToolStripMenuItem_Click);
            // 
            // vietnamRegionLicenseReportToolStripMenuItem
            // 
            this.vietnamRegionLicenseReportToolStripMenuItem.Name = "vietnamRegionLicenseReportToolStripMenuItem";
            this.vietnamRegionLicenseReportToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.vietnamRegionLicenseReportToolStripMenuItem.Text = "Vietnam Region License Report";
            this.vietnamRegionLicenseReportToolStripMenuItem.Click += new System.EventHandler(this.vietnamRegionLicenseReportToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 240);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Output folder";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(174, 237);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(502, 20);
            this.txtOutputFolder.TabIndex = 8;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(683, 235);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFolder.TabIndex = 9;
            this.btnSelectFolder.Text = "...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "VND to USD";
            // 
            // txtUsdRate
            // 
            this.txtUsdRate.Location = new System.Drawing.Point(176, 204);
            this.txtUsdRate.Name = "txtUsdRate";
            this.txtUsdRate.Size = new System.Drawing.Size(100, 20);
            this.txtUsdRate.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Vietnam Region License Report";
            // 
            // txtVietnamRegionLicense
            // 
            this.txtVietnamRegionLicense.Location = new System.Drawing.Point(176, 171);
            this.txtVietnamRegionLicense.Name = "txtVietnamRegionLicense";
            this.txtVietnamRegionLicense.Size = new System.Drawing.Size(500, 20);
            this.txtVietnamRegionLicense.TabIndex = 15;
            // 
            // btnSelectLicenseFile
            // 
            this.btnSelectLicenseFile.Location = new System.Drawing.Point(683, 170);
            this.btnSelectLicenseFile.Name = "btnSelectLicenseFile";
            this.btnSelectLicenseFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectLicenseFile.TabIndex = 16;
            this.btnSelectLicenseFile.Text = "...";
            this.btnSelectLicenseFile.UseVisualStyleBackColor = true;
            this.btnSelectLicenseFile.Click += new System.EventHandler(this.btnSelectLicenseFile_Click);
            // 
            // lblDownloadTemplateFile
            // 
            this.lblDownloadTemplateFile.AutoSize = true;
            this.lblDownloadTemplateFile.Location = new System.Drawing.Point(564, 65);
            this.lblDownloadTemplateFile.Name = "lblDownloadTemplateFile";
            this.lblDownloadTemplateFile.Size = new System.Drawing.Size(114, 13);
            this.lblDownloadTemplateFile.TabIndex = 19;
            this.lblDownloadTemplateFile.TabStop = true;
            this.lblDownloadTemplateFile.Text = "Download template file";
            this.lblDownloadTemplateFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDownloadTemplateFile_LinkClicked);
            // 
            // pnBasicSetting
            // 
            this.pnBasicSetting.Controls.Add(this.dtpEndShippedDate);
            this.pnBasicSetting.Controls.Add(this.dtpStartShippedDate);
            this.pnBasicSetting.Controls.Add(this.label8);
            this.pnBasicSetting.Controls.Add(this.label7);
            this.pnBasicSetting.Controls.Add(this.btnSelectSchools);
            this.pnBasicSetting.Controls.Add(this.btnClearSpecificSchools);
            this.pnBasicSetting.Controls.Add(this.label6);
            this.pnBasicSetting.Controls.Add(this.txtSpecificSchools);
            this.pnBasicSetting.Controls.Add(this.label2);
            this.pnBasicSetting.Controls.Add(this.chkZipfolder);
            this.pnBasicSetting.Controls.Add(this.txtInvoiceTemplatePath);
            this.pnBasicSetting.Controls.Add(this.lblDownloadTemplateFile);
            this.pnBasicSetting.Controls.Add(this.label1);
            this.pnBasicSetting.Controls.Add(this.btnOpenFileTemplate);
            this.pnBasicSetting.Controls.Add(this.btnSelectLicenseFile);
            this.pnBasicSetting.Controls.Add(this.label3);
            this.pnBasicSetting.Controls.Add(this.txtVietnamRegionLicense);
            this.pnBasicSetting.Controls.Add(this.txtOutputFolder);
            this.pnBasicSetting.Controls.Add(this.label5);
            this.pnBasicSetting.Controls.Add(this.btnSelectFolder);
            this.pnBasicSetting.Controls.Add(this.txtUsdRate);
            this.pnBasicSetting.Controls.Add(this.label4);
            this.pnBasicSetting.Location = new System.Drawing.Point(19, 36);
            this.pnBasicSetting.Name = "pnBasicSetting";
            this.pnBasicSetting.Size = new System.Drawing.Size(774, 316);
            this.pnBasicSetting.TabIndex = 20;
            this.pnBasicSetting.TabStop = false;
            this.pnBasicSetting.Text = "Basic Settings";
            // 
            // dtpEndShippedDate
            // 
            this.dtpEndShippedDate.Location = new System.Drawing.Point(492, 276);
            this.dtpEndShippedDate.Name = "dtpEndShippedDate";
            this.dtpEndShippedDate.Size = new System.Drawing.Size(184, 20);
            this.dtpEndShippedDate.TabIndex = 29;
            // 
            // dtpStartShippedDate
            // 
            this.dtpStartShippedDate.Location = new System.Drawing.Point(174, 276);
            this.dtpStartShippedDate.Name = "dtpStartShippedDate";
            this.dtpStartShippedDate.Size = new System.Drawing.Size(157, 20);
            this.dtpStartShippedDate.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(387, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "End shipped date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(73, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Start shipped date";
            // 
            // btnSelectSchools
            // 
            this.btnSelectSchools.Location = new System.Drawing.Point(684, 88);
            this.btnSelectSchools.Name = "btnSelectSchools";
            this.btnSelectSchools.Size = new System.Drawing.Size(75, 42);
            this.btnSelectSchools.TabIndex = 25;
            this.btnSelectSchools.Text = "Select schools";
            this.btnSelectSchools.UseVisualStyleBackColor = true;
            this.btnSelectSchools.Click += new System.EventHandler(this.btnSelectSchools_Click);
            // 
            // btnClearSpecificSchools
            // 
            this.btnClearSpecificSchools.Location = new System.Drawing.Point(683, 134);
            this.btnClearSpecificSchools.Name = "btnClearSpecificSchools";
            this.btnClearSpecificSchools.Size = new System.Drawing.Size(75, 23);
            this.btnClearSpecificSchools.TabIndex = 24;
            this.btnClearSpecificSchools.Text = "Clear";
            this.btnClearSpecificSchools.UseVisualStyleBackColor = true;
            this.btnClearSpecificSchools.Click += new System.EventHandler(this.btnClearSpecificSchools_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label6.Location = new System.Drawing.Point(114, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "(Optional)";
            // 
            // txtSpecificSchools
            // 
            this.txtSpecificSchools.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtSpecificSchools.Location = new System.Drawing.Point(176, 89);
            this.txtSpecificSchools.Multiline = true;
            this.txtSpecificSchools.Name = "txtSpecificSchools";
            this.txtSpecificSchools.ReadOnly = true;
            this.txtSpecificSchools.Size = new System.Drawing.Size(500, 68);
            this.txtSpecificSchools.TabIndex = 22;
            this.txtSpecificSchools.Click += new System.EventHandler(this.chkSpecificSchools_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Invoices for specific schools";
            // 
            // chkZipfolder
            // 
            this.chkZipfolder.AutoSize = true;
            this.chkZipfolder.Location = new System.Drawing.Point(303, 206);
            this.chkZipfolder.Name = "chkZipfolder";
            this.chkZipfolder.Size = new System.Drawing.Size(70, 17);
            this.chkZipfolder.TabIndex = 20;
            this.chkZipfolder.Text = "Zip folder";
            this.chkZipfolder.UseVisualStyleBackColor = true;
            // 
            // lblTurnOffTestMode
            // 
            this.lblTurnOffTestMode.AutoSize = true;
            this.lblTurnOffTestMode.LinkColor = System.Drawing.Color.Red;
            this.lblTurnOffTestMode.Location = new System.Drawing.Point(456, 365);
            this.lblTurnOffTestMode.Name = "lblTurnOffTestMode";
            this.lblTurnOffTestMode.Size = new System.Drawing.Size(93, 13);
            this.lblTurnOffTestMode.TabIndex = 21;
            this.lblTurnOffTestMode.TabStop = true;
            this.lblTurnOffTestMode.Text = "Turn off test mode";
            this.lblTurnOffTestMode.Visible = false;
            this.lblTurnOffTestMode.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblTurnOffTestMode_LinkClicked);
            // 
            // btnOpenInvoiceFolder
            // 
            this.btnOpenInvoiceFolder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnOpenInvoiceFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnOpenInvoiceFolder.Location = new System.Drawing.Point(232, 384);
            this.btnOpenInvoiceFolder.Name = "btnOpenInvoiceFolder";
            this.btnOpenInvoiceFolder.Size = new System.Drawing.Size(145, 45);
            this.btnOpenInvoiceFolder.TabIndex = 22;
            this.btnOpenInvoiceFolder.Text = "Open invoice folder";
            this.btnOpenInvoiceFolder.UseVisualStyleBackColor = false;
            this.btnOpenInvoiceFolder.Click += new System.EventHandler(this.btnOpenInvoiceFolder_Click);
            // 
            // FrmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 449);
            this.Controls.Add(this.btnOpenInvoiceFolder);
            this.Controls.Add(this.lblTurnOffTestMode);
            this.Controls.Add(this.pnBasicSetting);
            this.Controls.Add(this.btnCreateInvoice);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FrmMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grapeseed invoices";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnBasicSetting.ResumeLayout(false);
            this.pnBasicSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreateInvoice;
        private System.Windows.Forms.TextBox txtInvoiceTemplatePath;
        private System.Windows.Forms.Button btnOpenFileTemplate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuSettingForm;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsdRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVietnamRegionLicense;
        private System.Windows.Forms.Button btnSelectLicenseFile;
        private System.Windows.Forms.ToolStripMenuItem ignoreAccountManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogFolderToolStripMenuItem;
        private System.Windows.Forms.LinkLabel lblDownloadTemplateFile;
        private System.Windows.Forms.GroupBox pnBasicSetting;
        private System.Windows.Forms.ToolStripMenuItem templateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vietnamRegionLicenseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turnOnTestTemplateModeToolStripMenuItem;
        private System.Windows.Forms.LinkLabel lblTurnOffTestMode;
        private System.Windows.Forms.Button btnOpenInvoiceFolder;
        private System.Windows.Forms.CheckBox chkZipfolder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSpecificSchools;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClearSpecificSchools;
        private System.Windows.Forms.Button btnSelectSchools;
        private System.Windows.Forms.DateTimePicker dtpEndShippedDate;
        private System.Windows.Forms.DateTimePicker dtpStartShippedDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}

