﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using GrapeSeedInvoice.Code;
using GrapeSeedInvoice.DataProcessing;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice
{
    public partial class Login : Form
    {
        private ApiHelper apiHelper;
        private GrapeSeedCommonApi grapeSeedCommonApi;
        private FrmMainForm frmMainForm;

        public Login()
        {
            InitializeComponent();
            LoadRegistryValue();
        }

        private void btnLogin_Click(object sender, System.EventArgs e)
        {
            var testEnv = chkTestEnvironment.Checked;
            apiHelper = new ApiHelper(testEnv, txtEmail.Text, txtPassword.Text);
            grapeSeedCommonApi = new GrapeSeedCommonApi(testEnv, txtEmail.Text, txtPassword.Text);
            
            
            var threadInput = new Thread(ProcessData);
            threadInput.Start();
            
            // try
            // {
            //     var token = apiHelper.GetToken();
            //     if (string.IsNullOrWhiteSpace(token))
            //     {
            //         MessageHelper.Error("Invalid email or password");
            //     }
            //     else
            //     {
            //         if (CheckRole())
            //         {
            //             SaveRegistryValue();
            //             ShowMainProgram();   
            //         }
            //     }
            // }
            // catch (Exception exception)
            // {
            //     MessageHelper.Error(exception.Message);
            // }
        }

        private bool CheckRole()
        {
            var userProfile = grapeSeedCommonApi.GetUserProfile(txtEmail.Text);
            if (userProfile == null || userProfile?.Roles.Any() == false)
            {
                throw new Exception("Can not find the user information");
            }

            if (!userProfile.Roles.Contains("RegionAdmin") && !userProfile.Roles.Contains("AccountManager"))
            {
                throw new Exception("Only support for RegionAdmin & AccountManager");
            }

            return true;
        }

        private void LoadRegistryValue()
        {
            chkRemember.Checked = RegistryHelper.GetBool(RegistryKey.RememberMe);
            // chkTestEnvironment.Checked = RegistryHelper.GetBool(RegistryKey.UseTestApiServer);
            txtEmail.Text = RegistryHelper.GetString(RegistryKey.UserName);
            txtPassword.Text = RegistryHelper.GetString(RegistryKey.Password);
        }

        private void SaveRegistryValue()
        {
            // RegistryHelper.SetValue(RegistryKey.UseTestApiServer, chkTestEnvironment.Checked ? chkRemember.Checked.ToString() : "");
            if (chkRemember.Checked)
            {
                RegistryHelper.SetValue(RegistryKey.RememberMe, chkRemember.Checked.ToString());
                RegistryHelper.SetValue(RegistryKey.UserName, txtEmail.Text);
                RegistryHelper.SetValue(RegistryKey.Password, txtPassword.Text);
            }
            else
            {
                RegistryHelper.SetValue(RegistryKey.RememberMe, "");
                RegistryHelper.SetValue(RegistryKey.UserName, "");
                RegistryHelper.SetValue(RegistryKey.Password, "");
            }
        }

        private void ShowMainProgram()
        {
            this.Invoke((MethodInvoker)delegate
            {
                frmMainForm = new FrmMainForm(chkTestEnvironment.Checked, txtEmail.Text, txtPassword.Text);
                frmMainForm.FormClosing += delegate(object o, FormClosingEventArgs args)
                {
                    btnLogin.Enabled = true; 
                    btnLogin.Text = "Login";
                    this.Show();
                };
                frmMainForm.Show();
                this.Hide();
            });
        }

        #region Loading

        private void ProcessData()
        {
            try
            { 
                SetLoading(true);
        
                var token = apiHelper.GetToken();
                if (string.IsNullOrWhiteSpace(token))
                {
                    MessageHelper.Error("Invalid email or password");
                }
                else
                {
                    if (CheckRole())
                    {
                        SetLoading(false);
                        SaveRegistryValue();
                        ShowMainProgram();   
                    }
                }
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }


        private void SetLoading(bool showLoading)
        {
            if (showLoading)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    btnLogin.Enabled = false;
                    btnLogin.Text = "Login...";
                    this.Cursor = Cursors.WaitCursor;
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    btnLogin.Enabled = true; 
                    btnLogin.Text = "Login";
                    this.Cursor = Cursors.Default;
                });
            }
        }

        private void DisplayError(Exception ex)
        {
            this.Invoke((MethodInvoker)delegate
            {
                MessageHelper.Error(ex.Message);
                SetLoading(false);
            });
        }

        #endregion
    }
}