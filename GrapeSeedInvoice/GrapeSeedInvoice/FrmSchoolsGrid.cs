﻿using GrapeSeedInvoice.DataProcessing;
using GrapeSeedInvoice.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using GrapeSeedInvoice.DataProcessing.Models;

namespace GrapeSeedInvoice
{
    public partial class FrmSchoolsGrid : Form
    {
        private List<SchoolDto> gridDataSource { get; set; }

        public List<SchoolDto> SelectedSchools { get; set; }

        private bool isTestEnvironment { get; set; }
        
        private string UserName { get; set; }

        private string Password { get; set; }

        public FrmSchoolsGrid(string userName, string password)
        {
            InitializeComponent();
        }

        public void ShowAsDialog(List<SchoolDto> selectedSchools, bool isTestEnv)
        {
            isTestEnvironment = isTestEnv;
            lblIndicator.Text = isTestEnv ? "All schools (TEST DATA)" : "All schools (REAL DATA)";
            this.SelectedSchools = selectedSchools;
            BindingData(grvSelectedSchools, selectedSchools);
            LoadSchools();
            this.ShowDialog();
        }

        private bool LoadSchools()
        {
            try
            {
                var filePath = FolderHelper.CreateCurrentSubfolder("Data") + (isTestEnvironment ? "\\Schools_TEST.txt" : "\\Schools.txt");
                if (!FileHelper.IsExisted(filePath))
                    return false;

                var json = File.ReadAllText(filePath);
                if (string.IsNullOrWhiteSpace(json))
                    return false;

                var schoolData = JsonConvert.DeserializeObject<SchoolListDto>(json);
                gridDataSource = schoolData.Schools;
                this.BindingData(grvSchools, schoolData.Schools);
                return true;
            }
            catch (Exception e)
            {
                LogHelper.LogMessage("FrmSchoolsGrid.LoadSchools" + e.Message);
                return false;
            } 
        }

        private void btnSearchSchool_Click(object sender, EventArgs e)
        {
            if (gridDataSource == null)
            {
                MessageBox.Show("You have to pull data first", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var term = txtKeyword.Text.RemoveVietnameseSign();
            var searchData = gridDataSource.Where(x => x.id.Contains(term) ||
                                                       x.name.RemoveVietnameseSign().Contains(term) ||
                                                       x.trainerName?.RemoveVietnameseSign().Contains(term) == true ||
                                                       x.SchoolAddress?.RemoveVietnameseSign().Contains(term) == true).ToList();

            if (searchData?.Count > 0)
            {
                this.BindingData(grvSchools, searchData);
            }
            else
            {
                MessageBox.Show("Couldn't find any data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtKeyword.Text = "";
            if (gridDataSource?.Count > 0)
            {
                this.BindingData(grvSchools, gridDataSource);
            }
            else
            {
                LoadSchools();
            }
        }

        private void BindingData(DataGridView grdView, List<SchoolDto> data)
        {
            if (data == null || grdView == null)
                return;

            var bindingList = new BindingList<SchoolDto>(data);
            var source = new BindingSource(bindingList, null);
            grdView.DataSource = source;
        }

        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearchSchool_Click(sender, e);
            }
        }

        private void btnGetSelected_Click(object sender, EventArgs e)
        {
            var totalSelectedRows = grvSchools.SelectedRows.Count;

            if (SelectedSchools == null)
                SelectedSchools = new List<SchoolDto>();

            for (var i = 0; i < totalSelectedRows; i++)
            {
                var id = grvSchools.SelectedRows[i].Cells[0].Value?.ToString();
                var selectedSchool = gridDataSource.FirstOrDefault(x => x.id == id);
                if (selectedSchool != null && SelectedSchools.All(x => x.id != id))
                {
                    SelectedSchools.Add(selectedSchool);
                }
            }

            BindingData(grvSelectedSchools, SelectedSchools);
            //var bindingList = new BindingList<SchoolApiItemResponse>(SelectedSchools);
            //var source = new BindingSource(bindingList, null);
            //grvSelectedSchools.DataSource = source;
        }

        private void btnDeleteSelected_Click(object sender, EventArgs e)
        {
            var totalSelectedRows = grvSelectedSchools.SelectedRows.Count;
            for (var i = 0; i < totalSelectedRows; i++)
            {
                var id = grvSelectedSchools.SelectedRows[i].Cells[0].Value?.ToString();
                SelectedSchools.Remove(SelectedSchools.FirstOrDefault(x => x.id == id));
            }
            BindingData(grvSelectedSchools, SelectedSchools);
            //var bindingList = new BindingList<SchoolApiItemResponse>(SelectedSchools);
            //var source = new BindingSource(bindingList, null);
            //grvSelectedSchools.DataSource = source;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.SelectedSchools = new List<SchoolDto>();
            this.Dispose();
        }

        private void btnPullData_Click(object sender, EventArgs e)
        {
            var threadInput = new Thread(LoadSchoolsFromApi);
            threadInput.Start();
        }

        private void LoadSchoolsFromApi()
        {
            SetLoading(true);
            var data = new GrapeSeedCommonApi(isTestEnvironment, UserName, Password).GetSchools();
            if (data != null)
            {
                gridDataSource = data.Schools;
                this.Invoke((MethodInvoker)delegate
                {
                    this.BindingData(grvSchools, data.Schools);
                });
            }
            else
            {
                MessageBox.Show("Can not get schools", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SetLoading(false);
        }

        private void SetLoading(bool displayLoader, string messageLog = "")
        {
            if (displayLoader)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.Enabled = false;
                    this.Cursor = Cursors.WaitCursor;
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.Enabled = true;
                    this.Cursor = Cursors.Default;
                });
            }

            if (!string.IsNullOrWhiteSpace(messageLog))
            {
                LogHelper.LogMessage(messageLog);
            }
        }
    }
}
