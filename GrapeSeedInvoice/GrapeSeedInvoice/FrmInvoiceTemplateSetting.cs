﻿using GrapeSeedInvoice.Helper;
using GrapeSeedInvoice.Models;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GrapeSeedInvoice
{
    public partial class FrmInvoiceTemplateSetting : Form
    {
        public FrmInvoiceTemplateSetting()
        {
            InitializeComponent();
            LoadSetting();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
        }

        public sealed override Size MinimumSize
        {
            get => base.MinimumSize;
            set => base.MinimumSize = value;
        }

        public sealed override Size MaximumSize
        {
            get => base.MaximumSize;
            set => base.MaximumSize = value;
        }

        private void LoadSetting()
        {
            try
            { 
                var setting = RegistryHelper.GetObject(RegistryKey.InvoiceTemplateColumnRowSetting, new InvoiceTemplateColumnRowSetting());
                BindSettingToForm(setting);

            }
            catch (Exception e)
            {
                LogHelper.LogMessage("FrmInvoiceTemplateSetting.LoadSetting " + e.Message);
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSettings();
                this.Dispose();
            }
            catch (Exception ex)
            {
                LogHelper.LogMessage("FrmInvoiceTemplateSetting.btnSave_Click " + ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                var defaultSetting = new InvoiceTemplateColumnRowSetting();
                BindSettingToForm(defaultSetting);
                SaveSettings();
            }
            catch (Exception ex)
            {
                LogHelper.LogMessage("FrmInvoiceTemplateSetting.btnReset_Click " + ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveSettings()
        {
            var setting = new InvoiceTemplateColumnRowSetting()
            {
                InvoiceDateRow = GetRow(txtInvoiceDate.Text),
                InvoiceDateColumn = GetColumn(txtInvoiceDate.Text),
                AdjustmentColumn = GetColumn(txtManualAdjustment.Text),
                AddressRow = GetRow(txtAddress.Text),
                SchoolRow = GetRow(txtSchoolName.Text),
                SchoolColumn = GetColumn(txtSchoolName.Text),
                AddressColumn = GetColumn(txtAddress.Text),
                TotalPriceColumn = GetColumn(txtTotalPrice.Text),
                QuantityColumn = GetColumn(txtQuantityColumn.Text),
                ExchangeRateColumn = GetColumn(txtUsdExchangeRate.Text),
                ExchangeRateRow = GetRow(txtUsdExchangeRate.Text),
                PriceColumn = GetColumn(txtPrice.Text),
                StartFillDataRow = GetRow(txtStartRowIndex.Text),
                ProductColumn = GetColumn(txtProductName.Text),
                SheetName = txtSheetName.Text
            };

            var str = JsonConvert.SerializeObject(setting);
            RegistryHelper.SetValue(RegistryKey.InvoiceTemplateColumnRowSetting, str);
        }

        private void BindSettingToForm(InvoiceTemplateColumnRowSetting setting)
        {
            txtAddress.Text = setting.AddressRow + setting.AddressColumn;
            txtInvoiceDate.Text = setting.InvoiceDateRow + setting.InvoiceDateColumn;
            txtSchoolName.Text = setting.SchoolRow + setting.SchoolColumn;
            txtUsdExchangeRate.Text = setting.ExchangeRateRow + setting.ExchangeRateColumn;

            txtStartRowIndex.Text = setting.StartFillDataRow + "";
            txtQuantityColumn.Text = setting.QuantityColumn;
            txtManualAdjustment.Text = setting.AdjustmentColumn;
            txtPrice.Text = setting.PriceColumn;
            txtTotalPrice.Text = setting.TotalPriceColumn;
            txtProductName.Text = setting.ProductColumn;
            txtSheetName.Text = setting.SheetName;
        }

        private string GetColumn(string input)
        {
            return input.FirstOrDefault(x => "0123456789".Contains(x) == false) + "";
        }

        private int GetRow(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return 0;
            }

            var result = "";
            foreach (var number in input)
            {
                if ("0123456789".Contains(number.ToString()))
                {
                    result += number;
                }
            }

            return int.Parse(result);
        }
    }
}
