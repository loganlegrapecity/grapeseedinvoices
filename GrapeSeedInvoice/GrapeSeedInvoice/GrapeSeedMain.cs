﻿using GrapeSeedInvoice.DataProcessing;
using GrapeSeedInvoice.ExportInvoices;
using GrapeSeedInvoice.Helper;
using GrapeSeedInvoice.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Windows.Forms;
using GrapeSeedInvoice.DataProcessing.Models;

namespace GrapeSeedInvoice
{
    public partial class FrmMainForm : Form
    {
        private bool TestTemplateMode = false;

        private string InvoiceFolder { get; set; }

        private List<SchoolDto> SpecificSchools { get; set; }

        private bool IsTestEnvironment { get; set; }
        
        private string UserName { get; set; }

        private string Password { get; set; }

        public FrmMainForm(bool isTestEnv, string userName, string password)
        {
            InitializeComponent();
            this.IsTestEnvironment = isTestEnv;
            this.UserName = userName;
            this.Password = password;
            LoadRegistryValue();

            dtpEndShippedDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 23);
            dtpStartShippedDate.Value = dtpEndShippedDate.Value.AddMonths(-1);
        }

        #region Events

        private void vietnamRegionLicenseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmVietnamLicenseSetting().ShowDialog();
        }

        private void btnOpenInvoiceFolder_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(InvoiceFolder))
            {
                MessageBox.Show("Please generate invoices first", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            OpenFolder(InvoiceFolder);
        }

        private void btnSelectSchools_Click(object sender, EventArgs e)
        {
            var schoolForm = new FrmSchoolsGrid(UserName, Password);
            schoolForm.ShowAsDialog(SpecificSchools, this.IsTestEnvironment);
            SpecificSchools = schoolForm.SelectedSchools;
            txtSpecificSchools.Text = "";
            if (SpecificSchools?.Count > 0)
            {
                foreach (var school in SpecificSchools)
                {
                    txtSpecificSchools.Text += $"{school.name}({school.id})\r\n";
                }
            }
        }

        private void chkSpecificSchools_Click(object sender, EventArgs e)
        {
            btnSelectSchools_Click(sender, e);
        }

        private void btnClearSpecificSchools_Click(object sender, EventArgs e)
        {
            SpecificSchools = new List<SchoolDto>();
            txtSpecificSchools.Text = "";
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            var folderDlg = new FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };

            var result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtOutputFolder.Text = folderDlg.SelectedPath;
                Environment.SpecialFolder root = folderDlg.RootFolder;
            }
        }

        private void btnSelectLicenseFile_Click(object sender, EventArgs e)
        {
            var openExcelTemplate = OpenFileWindow();

            if (openExcelTemplate.ShowDialog() == DialogResult.OK)
            {
                txtVietnamRegionLicense.Text = openExcelTemplate.FileName;
            }
        }

        private void btnOpenFileTemplate_Click(object sender, EventArgs e)
        {
            var openExcelTemplate = OpenFileWindow();

            if (openExcelTemplate.ShowDialog() == DialogResult.OK)
            {
                txtInvoiceTemplatePath.Text = openExcelTemplate.FileName;
            }
        }

        private void btnCreateInvoice_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInvoiceTemplatePath.Text))
            {
                ShowWarningMessage("Invoice template is required");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtOutputFolder.Text))
            {
                ShowWarningMessage("Please select folder to save invoices");
                return;
            }

            if (!isValidNumber(txtUsdRate.Text))
            {
                ShowWarningMessage("Please enter USD exchange rate");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtVietnamRegionLicense.Text))
            {
                ShowWarningMessage("Please select file for Vietnam region license");
                return;
            }

            if (FileHelper.IsOpened(txtInvoiceTemplatePath.Text) || FileHelper.IsOpened(txtVietnamRegionLicense.Text))
            {
                ShowWarningMessage("Please close invoice template/license file before processing");
                return;
            }

            if (dtpStartShippedDate.Value > dtpEndShippedDate.Value)
            {
                ShowWarningMessage("Start shipped date must be less than or equal to end shipped date");
                return;
            }

            Thread threadInput = new Thread(ProcessData);
            threadInput.Start();
        }

        private void ignoreAccountManagersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new IgnoreAccountManager();
            frm.ShowDialog();
        }

        private void lblDownloadTemplateFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var document =
                "https://grapecity.sharepoint.com/:x:/r/teams/grapeseed/vn/sales/Shared%20Documents/Templates/TEMPLATEMonthlyInvoice_19_03_2020.xlsx?d=w458196fa29254220814083d05c5c1ff5&csf=1&e=PCWDW9";
            var sInfo = new ProcessStartInfo(document);
            Process.Start(sInfo);
        }

        private void invoiceTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmInvoiceTemplateSetting().ShowDialog();
        }

        private void turnOnTestTemplateModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestTemplateMode = !TestTemplateMode;
            turnOnTestTemplateModeToolStripMenuItem.Text =
                TestTemplateMode ? "Turn off test invoice template mode" : "Turn on test invoice template mode";
            if (TestTemplateMode)
            {
                btnCreateInvoice.Text = "Start test";
                btnCreateInvoice.BackColor = Color.Crimson;
                lblTurnOffTestMode.Visible = true;
            }
            else
            {
                btnCreateInvoice.Text = "Generate invoices";
                btnCreateInvoice.BackColor = Color.FromArgb(0, 120, 215);
                lblTurnOffTestMode.Visible = false;
            }
        }

        private void mnuSettingForm_Click(object sender, EventArgs e)
        {
            var frmSettingForm = new FrmPriceSetting();
            frmSettingForm.ShowDialog();
        }

        private void openLogFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var folderPath = LogHelper.GetLogFolderPath();
            OpenFolder(folderPath);
        }

        private void lblTurnOffTestMode_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            turnOnTestTemplateModeToolStripMenuItem_Click(sender, e);
        }

        #endregion

        #region Processing data

        private void ShowWarningMessage(string message)
        {
            MessageBox.Show(message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void ProcessData()
        {
            try
            {
                LogHelper.LogMessage("___________________________________");
                SetLoading(true, "Start processing Data");
                var watch = System.Diagnostics.Stopwatch.StartNew();
                SaveInputData();

                var setting = new SettingForProcessingDto()
                {
                    Password = this.Password,
                    UserName = this.UserName,
                    SpecificSchools = SpecificSchools,
                    IsTestEnv = this.IsTestEnvironment,
                    LicenseExcelFile = txtVietnamRegionLicense.Text,
                    EndShippedDate = dtpEndShippedDate.Value,
                    StartShippedDate = dtpStartShippedDate.Value,
                    UsdExchangeRate = float.Parse(txtUsdRate.Text)
                };
                var data = TestTemplateMode ? FakeDataForGenerateInvoicesTesting() : new DataSource().GetData(setting);
                if (data == null || data?.Count == 0)
                {
                    SetLoading(false, "There is no data for generating invoice");
                    MessageBox.Show("There is no data for generating invoice", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var generateInvoiceResult = GenerateInvoices(data);

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                var minutes = $"{TimeSpan.FromMilliseconds(elapsedMs).TotalMinutes:0.##}";
                var message =
                    $"Finished processing {generateInvoiceResult.Item1} invoice(s) \n\rTotal time: {minutes} minutes\n\rClick OK to open invoices folder";
                InvoiceFolder = generateInvoiceResult.Item2;
                SetLoading(false, message);

                //this.Invoke((MethodInvoker)delegate
                //{
                //    var finalForm = new FrmFinalResult();
                //    finalForm.BindingResultData(minutes, totalCreatedInvoice.Item1, totalCreatedInvoice.Item2);
                //    finalForm.ShowDialog();

                //});

                var messageResult = MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (messageResult == DialogResult.OK)
                {
                    OpenFolder(generateInvoiceResult.Item2);
                }
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        #endregion

        #region Helper

        private void OpenFolder(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                var startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe",
                    WindowStyle = ProcessWindowStyle.Maximized
                };

                Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show($"{folderPath} Directory does not exist!");
            }
        }

        private List<AccountManager> FakeDataForGenerateInvoicesTesting()
        {
            var invoiceProducts = new List<InvoiceProduct>();
            for (var i = 1; i <= 7; i++)
            {
                invoiceProducts.Add(new InvoiceProduct($"Test product name {i}", 2 * i, 4.4f * i)
                {
                    Adjustment = i * 4
                });
            }

            float.TryParse(txtUsdRate.Text, out float usdRate);
            if (usdRate <= 0)
                usdRate = 23.2f;

            return new List<AccountManager>()
            {
                new AccountManager()
                {
                    Email = "Logan.le@gapecity.com",
                    Name = "Logan",
                    Schools = new List<School>()
                    {
                        new School()
                        {
                            Name = "Hanoi university of science",
                            Address = "Thanh Xuan, Ha Noi",
                            InvoiceData = new InvoiceData()
                            {
                                InvoiceDate = DateTime.Now.ToShortDateString(),
                                Address = "Thanh Xuan, Ha Noi",
                                SchoolName = "Hanoi university of science",
                                UsdExchangeRate = usdRate,
                                Products = invoiceProducts
                            }
                        }
                    }
                }
            };
        }

        private Tuple<int, string> GenerateInvoices(List<AccountManager> data)
        {
            var today = $"{DateTime.Now.Year}_{DateTime.Now.ToString("MMM", CultureInfo.InvariantCulture)}_{DateTime.Now.Day}";
            var template = txtInvoiceTemplatePath.Text;
            var subFolder = $"Invoices_{today}";
            var outOut = $"{txtOutputFolder.Text}\\{subFolder}";
            var usdRate = float.Parse(txtUsdRate.Text);
            var totalCreatedInvoices = 0;
            var setting = RegistryHelper.GetObject(RegistryKey.InvoiceTemplateColumnRowSetting, new InvoiceTemplateColumnRowSetting());

            var folder = new DirectoryInfo(outOut);
            if (folder.Exists)
            {
                if (!FolderHelper.IsDirectoryEmpty(outOut))
                {
                    var index = 1;
                    while (true)
                    {
                        var newPath = $"{txtOutputFolder.Text}\\{subFolder}({index})";
                        var newFolder = new DirectoryInfo(newPath);
                        if (newFolder.Exists)
                        {
                            index++;
                        }
                        else
                        {
                            newFolder.Create();
                            outOut = newPath;
                            break;
                        }
                    }
                }
            }
            else
            {
                FolderHelper.CreateFolder(outOut);
            }

            //TODO: check xem có ai trùng tên ko ?

            foreach (var manager in data)
            {
                if (manager.Schools == null || manager.Schools.Count == 0)
                    continue;

                var folderPath = $"{outOut}\\{manager.Name}";
                FolderHelper.CreateFolder(folderPath);

                foreach (var school in manager.Schools)
                {
                    if (school.InvoiceData == null)
                        continue;

                    var invoiceFileName = $"{school.Name}_{today}.xlsx";
                    school.InvoiceData.UsdExchangeRate = usdRate;
                    var success = new InvoiceExport().FillData(school.InvoiceData,
                        template,
                        setting.SheetName,
                        $"{folderPath}\\{invoiceFileName}");
                    if (success)
                        totalCreatedInvoices += 1;
                }

                if (chkZipfolder.Checked)
                {
                    try
                    {
                        ZipFile.CreateFromDirectory(folderPath, $"{folderPath}.zip");
                        new DirectoryInfo(folderPath).Delete(true);
                    }
                    catch (Exception e)
                    {
                        LogHelper.LogMessage($"Zip folder named {e.Message}");
                    }
                }
            }

            return new Tuple<int, string>(totalCreatedInvoices, outOut);
        }

        private void SaveInputData()
        {
            RegistryHelper.SetValue(RegistryKey.InvoiceTemplate, txtInvoiceTemplatePath.Text);
            RegistryHelper.SetValue(RegistryKey.LicenseFile, txtVietnamRegionLicense.Text);
            RegistryHelper.SetValue(RegistryKey.OutputInvoiceFolder, txtOutputFolder.Text);
            RegistryHelper.SetValue(RegistryKey.UsdExchangeRate, txtUsdRate.Text);
            // RegistryHelper.SetValue(RegistryKey.UseTestApiServer, chkUseTestApiServer.Checked.ToString());
            RegistryHelper.SetValue(RegistryKey.ZipFolder, chkZipfolder.Checked.ToString());
        }

        private void LoadRegistryValue()
        {
            var template = RegistryHelper.GetString(RegistryKey.InvoiceTemplate);
            if (FileHelper.IsExisted(template))
                txtInvoiceTemplatePath.Text = template;

            var outPut = RegistryHelper.GetString(RegistryKey.OutputInvoiceFolder);
            if (FolderHelper.IsExisted(outPut))
                txtOutputFolder.Text = outPut;

            var license = RegistryHelper.GetString(RegistryKey.LicenseFile);
            if (FileHelper.IsExisted(license))
                txtVietnamRegionLicense.Text = license;

            chkZipfolder.Checked = RegistryHelper.GetBool(RegistryKey.ZipFolder);
            txtUsdRate.Text = RegistryHelper.GetString(RegistryKey.UsdExchangeRate);
            // chkUseTestApiServer.Checked = RegistryHelper.GetBool(RegistryKey.UseTestApiServer);
        }

        private void SetLoading(bool displayLoader, string messageLog = "")
        {
            if (displayLoader)
            {
                this.Invoke((MethodInvoker) delegate
                {
                    pnBasicSetting.Enabled = false;
                    menuStrip1.Enabled = false;
                    btnCreateInvoice.Text = "One moment please...";
                    btnCreateInvoice.Enabled = false;
                    btnOpenInvoiceFolder.Enabled = false;
                    this.Cursor = Cursors.WaitCursor;
                });
            }
            else
            {
                this.Invoke((MethodInvoker) delegate
                {
                    pnBasicSetting.Enabled = true;
                    menuStrip1.Enabled = true;
                    btnCreateInvoice.Text = TestTemplateMode ? "Start test" : "Create invoices";
                    btnCreateInvoice.Enabled = true;
                    btnOpenInvoiceFolder.Enabled = true;
                    this.Cursor = Cursors.Default;
                });
            }

            if (!string.IsNullOrWhiteSpace(messageLog))
            {
                LogHelper.LogMessage(messageLog);
            }
        }

        private void DisplayError(Exception ex)
        {
            this.Invoke((MethodInvoker) delegate
            {
                LogHelper.LogMessage(ex.Message);
                MessageBox.Show("The below error occurred while processing the request: \n\r \n\r" + ex.Message);
                SetLoading(false);
            });
        }

        private bool isValidNumber(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return false;

            float.TryParse(input, out float number);
            if (number <= 0)
                return false;

            return true;
        }

        private OpenFileDialog OpenFileWindow()
        {
            return new OpenFileDialog
            {
                InitialDirectory = @"D:\",
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "Excel files (*.xlsx)|*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
        }

        #endregion
    }
}