﻿using GrapeSeedInvoice.Models;
using System.Collections.Generic;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice.ExportInvoices
{
    public class InvoiceExport
    {
        public bool FillData(InvoiceData invoiceData, string templateExcelPath, string sheetNameTemplateExcelFile, string newExcelPath)
        {
            var setting = RegistryHelper.GetObject(RegistryKey.InvoiceTemplateColumnRowSetting, new InvoiceTemplateColumnRowSetting());

            var cellValues = new List<CellValue>
            {
                new CellValue(setting.InvoiceDateRow, setting.InvoiceDateColumn, invoiceData.InvoiceDate),
                new CellValue(setting.SchoolRow, setting.SchoolColumn, invoiceData.SchoolName),
                new CellValue(setting.AddressRow, setting.AddressColumn, invoiceData.Address),
                new CellValue(setting.ExchangeRateRow, setting.ExchangeRateColumn, invoiceData.UsdExchangeRate + "")
            };

            var row = setting.StartFillDataRow; //row start to fill data
            foreach (var item in invoiceData.Products)
            {
                cellValues.Add(new CellValue(row, setting.ProductColumn, item.ProductName));
                cellValues.Add(new CellValue(row, setting.QuantityColumn, item.Quantity + ""));
                cellValues.Add(new CellValue(row, setting.AdjustmentColumn, item.Adjustment + "")
                {
                    NumberFormat = "General"
                });
                cellValues.Add(new CellValue(row, setting.PriceColumn, item.Price + ""));

                cellValues.Add(new CellValue(row, setting.TotalPriceColumn, item.TotalPrice + "")
                {
                    Formula = $"=IFERROR(({setting.QuantityColumn}{row}+{setting.AdjustmentColumn}{row})*{setting.PriceColumn}{row}*${setting.ExchangeRateColumn}${setting.ExchangeRateRow},\"\")"
                });

                row++;
            }
            new ExcelHandle().FillData(cellValues, templateExcelPath, sheetNameTemplateExcelFile, newExcelPath, setting.StartFillDataRow, invoiceData.Products.Count);
            return true;
        }
    }
}
