﻿using GrapeSeedInvoice.Models;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using GrapeSeedInvoice.Helper;

namespace GrapeSeedInvoice.ExportInvoices
{
    public class ExcelHandle
    {
        public void FillData(List<CellValue> cells, string excelTemplateTemplate, string sheetName, string outPutFile, int startRowToFillData, int totalProducts)
        {
            var app = new Microsoft.Office.Interop.Excel.Application
            {
                Visible = false
            };

            Microsoft.Office.Interop.Excel.Workbook workbook = null;
            Microsoft.Office.Interop.Excel.Worksheet worksheet = null;

            var setting = RegistryHelper.GetObject(RegistryKey.InvoiceTemplateColumnRowSetting, new InvoiceTemplateColumnRowSetting());

            var totalExtraRows = totalProducts + 3; // add few more blank lines

            try
            {
                var f = new FileInfo(excelTemplateTemplate);
                if (!f.Exists)
                {
                    throw new Exception("Excel template does not exist");
                }

                CopyTemplateFile(excelTemplateTemplate, outPutFile);

                workbook = app.Workbooks.Open(outPutFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[sheetName];

                // add more rows
                for (var i = 0; i < totalExtraRows; i++)
                {
                    worksheet.Rows[startRowToFillData + 1].Insert(); //start row to insert row
                }

                // fill data into row
                foreach (var cell in cells)
                {
                    worksheet.Cells[cell.Line, cell.Column] = cell.Value;
                    if (!string.IsNullOrWhiteSpace(cell.Formula))
                    {
                        Range rng = worksheet.Range[$"{cell.Column}{cell.Line}"];
                        rng.Formula = cell.Formula;
                    }

                    // format định dạng cho số âm vì 1 số máy mở lên số âm sẽ bị để trong ngoặc tròn
                    if (!string.IsNullOrWhiteSpace(cell.NumberFormat))
                    {
                        worksheet.Cells[cell.Line, cell.Column].NumberFormat = cell.NumberFormat;
                    }

                    if (cell.Value?.IsNegativeNumber() == true)
                    {
                        worksheet.Cells[cell.Line, cell.Column].Font.Color = Microsoft.Office.Interop.Excel.XlRgbColor.rgbRed;
                    }
                }

                // add summary for the last row
                var summaryLine = totalExtraRows + startRowToFillData + 1;
                var summaryQuantityRow = worksheet.Range[$"{setting.QuantityColumn}{summaryLine}"];
                summaryQuantityRow.Formula = $"=SUM({setting.QuantityColumn}{startRowToFillData}:{setting.QuantityColumn}{startRowToFillData + totalExtraRows})";
                worksheet.Cells[summaryLine, $"{setting.QuantityColumn}"].Interior.Color = Microsoft.Office.Interop.Excel.XlRgbColor.rgbLightGreen;

                var summaryPriceRow = worksheet.Range[$"{setting.TotalPriceColumn}{summaryLine}"];
                summaryPriceRow.Formula = $"=SUM({setting.TotalPriceColumn}{startRowToFillData}:{setting.TotalPriceColumn}{startRowToFillData + totalExtraRows})";
                worksheet.Cells[summaryLine, $"{setting.TotalPriceColumn}"].Interior.Color = Microsoft.Office.Interop.Excel.XlRgbColor.rgbLightGreen;

                workbook.Close(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                app.Quit();
                if (worksheet != null)
                    Marshal.FinalReleaseComObject(worksheet);
                if (workbook != null)
                    Marshal.FinalReleaseComObject(workbook);

                Marshal.FinalReleaseComObject(app);
            }
        }

        private void CopyTemplateFile(string templatePath, string newPath)
        {
            // delete if existed
            var f = new FileInfo(newPath);
            if (f.Exists)
                f.Delete();

            var original = new FileInfo(templatePath);
            if (!original.Exists)
                throw new Exception("Invoice template does not existed");

            original.CopyTo(newPath);
        }
    }
}
